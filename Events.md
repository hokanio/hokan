# Events Diagram

```

              |                                                    HOKAN
              |
    OS FS   --|--> (notify/event.Log{})
   (notify) --|--> (notify/event.Event{                                                                 bus
              |                  FileAdded,             =>                    -> publish FileAdded     ----->  subscribe FileAdded
              |                  FileRemoved,           =>  StartFileWatcher  -> publish FileRemoved   ----->  subscribe FileRemoved
              |                  FileModified,          =>   (fan out 1:n)    -> publish FileChanged   ----->  subscribe FileChanged
              |  FileRenamedOldName+FileRenamedNewName, =>                    -> publish FileRenamed   ----->  subscribe FileRenamed
              |                }) 
              |
```

## Flow

* Use notify/event to get all file change events for the specific directory
* pkg/watcher.StartFileWatcher will map, check, and publish these events to a message bus
* pkg/backup/event package implements listeners for these events and will apply the changes from the file system to the backup
* pkg/backup/minio implements functions to store files into minIO
* pkg/backup/local will backup files using local file system
* pkg/backup/void is a backup implementation for debugging and will only print out events and file names
* currently only one backup implementation will be used, but it is also possible to use many different backup systems at the same time