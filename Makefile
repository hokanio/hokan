BASEDIR       := ${CURDIR}
TMP			  := ${BASEDIR}/tmp
LOCAL_BIN     := ${TMP}/bin

interface := interface {
awk_print := {print $$2}
MOCK_PACKAGES := $(shell grep '$(interface)' pkg/core/* | awk '$(awk_print)' | paste -sd ',' -)

.PHONY: run
run:
	cd cmd/hokan; go run main.go application.go inject_server.go inject_store.go inject_watcher.go inject_backup.go wire_gen.go

.PHONY: build
build:
	cd cmd/hokan; go build  -race -o ../../bin/hokan

.PHONY: wire
wire:
	go get github.com/google/wire/cmd/wire

# run this command after changing something in cmd/hokan/inject_*
.PHONY: generate
generate: wire mockgen
	go generate	./...

.PHONY: install-mockgen
install-mockgen:
	mkdir -p mocks
	GOPATH=${TMP} GOBIN=${LOCAL_BIN} go install github.com/golang/mock/mockgen@v1.6.0

.PHONY: mockgen
mockgen: install-mockgen
	@mkdir -p {LOCAL_BIN}
	${LOCAL_BIN}/mockgen -destination=mocks/mock_gen.go -package=mocks gitlab.com/hokanio/hokan/pkg/core ${MOCK_PACKAGES}

install-golangci-lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b ${LOCAL_BIN} v1.24.0

.PHONY: lint
lint: install-golangci-lint
	${LOCAL_BIN}/golangci-lint run

.PHONY: test
test: mockgen
	go test -timeout 10s -race -cover ./...

.PHONY: clean
clean:
	rm -rf mocks/
	rm -rf ${LOCAL_BIN}
	rm -rf tmp/
	rm -rf bin/