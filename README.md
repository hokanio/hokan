# Hokan

The idea of Hokan (保管 safekeeping) is to store changed files automatically to some cloud or whatever is implemented as a target storage provider. I use native windows/linux API to get notifications on file changes (with some CGO or WinAPI,  see [notify](https://gitlab.com/hokanio/notify)). So it's a kinda backup/disaster recovery tool for your personal files for Windows and Linux. Current implementation supports [MinIO](https://min.io/) and local file system.

Disclaimer: This version is unstable and heavily under development, don't use it for anything!

## Running

You need to check out the project and build/run it locally (you need to have working Golang environment):

```
git clone https://gitlab.com/hokanio/hokan.git
cd hokan
make run 
# on windows you may run `make.bat run`
```

## Configuration

You need to rename `cmd/hokan/config/config.exampl.yaml` to `cmd/hokan/config/config.yaml` before start the service. Current config looks like this:
```
storage_name: "minio"
minio: 
    access_key_id: hokan.igor
    secret_access_key: $SECRET
    endpoint: 192.168.0.141:9000
    use_ssl: false
local:
    path: "/home/igor/backup"
```

* storage_name: `minio` or `local`
* minio: add your credentials and endpoint for the local MinIO instalation
* path: if you want your files to be backuped locally just add a local path here

Note: this configuration part is only for the local development and it will be reworked in the future.

## Use cases

* notification about file change is fired from the file system. We send the file to the backup storage and store the information about it in the local key-value storage. So we know which file is stored where.
* user adds a new folder to the backup, we send all files to the backup
* on the new start we scan all the folders and compare file info with what we have stored in the local key0value, if something changed we send the file to backup. If a new file is there, we send it to backup.
* in the local key-value storage we store file path, timestamp, and checksum for every file we send to backup
* REST API endpoints are used to communicate and configure the application. Will be used for UI later.

## Working with REST API

REST API will be used for web/electron-based UI later. For now `curl` is the UI :)

* Add a new directory to the backup:
```
curl -v -d '[{"active":true, "path":"/home/igor/test", "recursive": true}]' localhost:8081/api/directories
```

* List all directories in the backup:
```
curl -v http://localhost:8081/api/directories

{
  "directories": [
    {
      "id": "26Kl4wf7MjX88jSrfJao49KcJBU",
      "path": "/home/igor/test",
      "recursive": true,
      "machine": "odaiba",
      "storage_name": "minio",
      "ignore": null
    }
  ],
  "links": [
    {
      "href": "/api/directories/",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "/api/directories/",
      "rel": "addDir",
      "method": "POST"
    },
    {
      "href": "/api/directories/{dirID}",
      "rel": "getDir",
      "method": "GET"
    }
  ],
  "meta": {
    "total_items": 1
  }
}
```

* Get information about the directory in the backup
```
curl http://localhost:8081/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU

{
  "directory": {
    "id": "26Kl4wf7MjX88jSrfJao49KcJBU",
    "path": "/home/igor/test",
    "recursive": true,
    "machine": "odaiba",
    "storage_name": "minio",
    "ignore": null
  },
  "stats": {
    "path": "/home/igor/test",
    "os": "linux",
    "total-files": 3,
    "total-dirs": 1,
    "total-size": 13,
    "total-size-h": "13 B"
  },
  "links": [
    {
      "href": "/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU",
      "rel": "delete",
      "method": "DELETE"
    },
    {
      "href": "/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU/files",
      "rel": "files",
      "method": "GET"
    }
  ]
}
```

* List all files from one backup:
```
curl http://localhost:8081/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU/files

{
  "files": [
    {
      "id": "26Kl4zuXlRtDdJybHCTYbWlEnDd",
      "path": "/home/igor/test/zorg.txt",
      "restore": "localhost:8081/api/files/26Kl4zuXlRtDdJybHCTYbWlEnDd/restore",
      "info": {
        "name": "zorg.txt",
        "size": 5,
        "mod-time": "2022-03-13T01:00:17.427263416+01:00"
      },
      "deleted": true
    },
    {
      "id": "26Kl50ZT0o2FJdd52XvPW8afOa2",
      "path": "/home/igor/test/foo.txt",
      "restore": "localhost:8081/api/files/26Kl50ZT0o2FJdd52XvPW8afOa2/restore",
      "info": {
        "name": "foo.txt",
        "size": 4,
        "mod-time": "2022-03-13T01:07:32.552990126+01:00"
      },
      "deleted": false
    },
    {
      "id": "26Kl52cHFFod6IKlLJ0CYrLupUj",
      "path": "/home/igor/test/bar.txt",
      "restore": "localhost:8081/api/files/26Kl52cHFFod6IKlLJ0CYrLupUj/restore",
      "info": {
        "name": "bar.txt",
        "size": 4,
        "mod-time": "2022-03-13T01:04:59.984378531+01:00"
      },
      "deleted": false
    }
  ],
  "links": [
    {
      "href": "/api/directories/26Kl4wf7MjX88jSrfJao49KcJBU/files",
      "rel": "self",
      "method": "GET"
    }
  ],
  "meta": {
    "total_items": 3
  }
}
```

* Restore a file in the original directory:
```
curl -XPOST localhost:8081/api/files/26Kl4zuXlRtDdJybHCTYbWlEnDd/restore                               

"OK"
```