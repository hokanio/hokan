package config

import (
	"fmt"
	"os"
	"os/user"
	"path"
	"strings"

	"github.com/cip8/autoname"
	"github.com/segmentio/ksuid"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	defaultStorageName = "void"

	VoidStorageName  = "void"
	LocalStorageName = "local"
	MinioStorageName = "minio"

	defaultLocalStoragePath = "backup"

	defaultConfigFileName = "config"
	defaultConfigPath     = "config"
)

func configOrDefaultBackup(c *Config) {
	if err := readConfigFile(c); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; use default config
			defaultBackup(c)
		} else {
			// Config file was found but another error was produced
			log.WithError(err).Fatal("Error reading config file")
		}
	}
}

func defaultBackup(c *Config) {
	c.Backup = getDefaultBackup(defaultStorageName)
	c.Backup.DefaultConfig = true
}

func getDefaultBackup(name string) Backup {
	switch name {
	case VoidStorageName:
		return defaultViodBackup()
	case LocalStorageName:
		return defaultLocalBackup()
	case MinioStorageName:
		return defaultEnvMinIOBackup()
	}

	return defaultViodBackup()
}

func defaultViodBackup() Backup {
	return Backup{
		StorageName: VoidStorageName,
	}
}

func defaultLocalBackup() Backup {
	var localPath string
	if IsWindows() {
		localPath = path.Join("c:", defaultLocalStoragePath)
	} else {
		localPath = path.Join("~", defaultLocalStoragePath)
	}

	return Backup{
		StorageName: LocalStorageName,
		Local: LocalConf{
			Path: localPath,
		},
	}
}

func defaultEnvMinIOBackup() Backup {
	return Backup{
		StorageName: MinioStorageName,
		MinIO: MinIOConf{
			Endpoint:        os.Getenv("MINIO_ENDPOIT"),
			AccessKeyID:     os.Getenv("MINIO_ID"),
			SecretAccessKey: os.Getenv("MINIO_KEY"),
			UseSSL:          false,
		},
	}
}

func readConfigFile(c *Config) error {
	viper.SetConfigType("yml")
	if c.Backup.configName == "" {
		c.Backup.configName = defaultConfigFileName
	}
	viper.SetConfigName(c.Backup.configName)
	if c.Backup.configPath == "" {
		c.Backup.configPath = defaultConfigPath
	}
	viper.AddConfigPath(c.Backup.configPath)
	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	if err := viper.Unmarshal(&c.Backup); err != nil {
		return err
	}

	switch c.Backup.StorageName {
	case MinioStorageName:
		c.Backup.IgnoreFiles = c.Backup.MinIO.IgnoreFiles
	case LocalStorageName:
		c.Backup.IgnoreFiles = c.Backup.Local.IgnoreFiles
	}

	c.Backup.LocalMachineName = localMachineName()

	return nil
}

func localMachineName() string {
	machine, err := os.Hostname()
	if err != nil {
		machine = autoname.Generate()
	}
	id := strings.ToLower(ksuid.New().String())
	userName := userName()
	return fmt.Sprintf("%s-%s-%s", machine, userName, id[:4])
}

func userName() string {
	userName := "user"
	u, err := user.Current()
	if err == nil {
		userName = strings.ToLower(u.Name)
	}
	userName = strings.ReplaceAll(userName, " ", "-")
	strings.TrimSpace(userName)
	return userName
}
