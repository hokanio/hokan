package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_defaultConfig(t *testing.T) {
	c := &Config{
		Backup: Backup{
			configPath: ".",
			configName: "config.exampl.yaml",
		},
	}
	err := readConfigFile(c)
	assert.NoError(t, err)
	assert.Equal(t, "minio", c.Backup.StorageName)
	assert.Equal(t, "localhost", c.Backup.MinIO.Endpoint)
	assert.Equal(t, "secret", c.Backup.MinIO.SecretAccessKey)
	assert.Equal(t, "keyid", c.Backup.MinIO.AccessKeyID)
	assert.Equal(t, false, c.Backup.MinIO.UseSSL)
}

func Test_defaultBackup(t *testing.T) {
	t.Run("void default backup", func(t *testing.T) {
		c := getDefaultBackup(VoidStorageName)
		assert.Equal(t, VoidStorageName, c.StorageName)
	})

	t.Run("local default backup", func(t *testing.T) {
		c := getDefaultBackup(LocalStorageName)
		assert.Equal(t, LocalStorageName, c.StorageName)
	})

	t.Run("minio default backup", func(t *testing.T) {
		c := getDefaultBackup(MinioStorageName)
		assert.Equal(t, MinioStorageName, c.StorageName)
	})
}
