package config

import (
	"os"
	"path/filepath"

	home "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
)

const (
	dbFileMode = 0755
	dbFileName = "store.db"
	dbFilePath = ".hokan"
)

// we store in the local storage information about backup state
func defaultStore(c *Config) {
	if c.Database.Path == "" {
		homeDir, _ := home.Dir()
		appDir := filepath.Join(homeDir, dbFilePath)
		createDirIfNotExist(appDir)
		c.Database.Path = filepath.Join(appDir, dbFileName)
	}
}

func createDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, dbFileMode)
		if err != nil {
			log.Fatal("cannot create application directory")
		}
	}
}
