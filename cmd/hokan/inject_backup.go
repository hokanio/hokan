package main

import (
	"context"

	"github.com/google/wire"

	"gitlab.com/hokanio/hokan/cmd/hokan/config"
	"gitlab.com/hokanio/hokan/pkg/backup"
	"gitlab.com/hokanio/hokan/pkg/core"
)

var backupSet = wire.NewSet(
	provideBackup,
	provideBackupOptions,
	proviceBackupResultChan,
)

func provideBackupOptions(conf config.Config) core.BackupOptions {
	switch conf.Backup.StorageName {
	case config.LocalStorageName:
		return core.BackupOptions{
			LocalMachineName: conf.Backup.StorageName,
			Name:             conf.Backup.StorageName,
			TargetURL:        conf.Backup.Local.Path,
		}
	case config.MinioStorageName:
		return core.BackupOptions{
			LocalMachineName: conf.Backup.LocalMachineName,
			Name:             conf.Backup.StorageName,
			TargetURL:        conf.Backup.MinIO.Endpoint,
			AccessKeyID:      conf.Backup.MinIO.AccessKeyID,
			SecretAccessKey:  conf.Backup.MinIO.SecretAccessKey,
		}

	default:
		return core.BackupOptions{
			LocalMachineName: conf.Backup.LocalMachineName,
			Name:             conf.Backup.StorageName,
		}
	}
}

func provideBackup(ctx context.Context,
	configStore core.ConfigStore,
	fileStore core.FileStore,
	events core.EventCreator,
	results chan core.BackupResult,
	backupOptions core.BackupOptions) (core.Backup, error) {
	return backup.New(ctx, configStore, fileStore, events, results, &backupOptions)
}

func proviceBackupResultChan() chan core.BackupResult {
	return make(chan core.BackupResult)
}
