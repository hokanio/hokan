package main

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/google/wire"

	"gitlab.com/hokanio/hokan/cmd/hokan/config"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/event"
	"gitlab.com/hokanio/hokan/pkg/handler/api"
	"gitlab.com/hokanio/hokan/pkg/handler/events"
	"gitlab.com/hokanio/hokan/pkg/handler/health"
	"gitlab.com/hokanio/hokan/pkg/handler/web"
	"gitlab.com/hokanio/hokan/pkg/server"
	"gitlab.com/hokanio/hokan/pkg/sse"
)

type healthzHandler http.Handler

// wire set for loading the server.
var serverSet = wire.NewSet(
	apiServerProvider,
	eventsServerProvider,
	provideHealthz,
	provideWebHandler,
	provideRouter,
	provideServer,
	provideEventCreator,
	provideServerSideEventCreator,
)

func provideWebHandler(configStore core.ConfigStore, directoryStore core.DirectoryStore, backupOptions core.BackupOptions) *web.Server {
	return web.New(configStore, directoryStore, backupOptions)
}

func apiServerProvider(
	fileStore core.FileStore,
	dirStore core.DirectoryStore,
	events core.EventCreator) *api.Server {
	return api.New(fileStore, dirStore, events)
}

func eventsServerProvider(sseCreator core.ServerSideEventCreator) *events.Server {
	return events.New(sseCreator)
}

func provideRouter(apiHandler *api.Server,
	webHandler *web.Server,
	serverEventsHandler *events.Server,
	healthz healthzHandler) http.Handler {
	r := chi.NewRouter()
	r.Mount("/healthz", healthz)
	r.Mount("/api", apiHandler.Handler())
	r.Mount("/sse", serverEventsHandler.Handler())
	r.Mount("/", webHandler.Handler())
	return r
}

func provideHealthz() healthzHandler {
	v := health.New()
	return healthzHandler(v)
}

func provideServer(handler http.Handler, config config.Config) *server.Server {
	return &server.Server{
		Addr:    config.Server.Addr,
		Host:    config.Server.Host,
		Handler: handler,
	}
}

func provideEventCreator() core.EventCreator {
	return event.New(event.Config{})
}

func provideServerSideEventCreator(ctx context.Context, results chan core.BackupResult) core.ServerSideEventCreator {
	return sse.New(ctx, results)
}
