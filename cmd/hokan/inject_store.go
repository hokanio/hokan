package main

import (
	"github.com/google/wire"

	"gitlab.com/hokanio/hokan/cmd/hokan/config"

	"gitlab.com/hokanio/hokan/pkg/core"
	configStore "gitlab.com/hokanio/hokan/pkg/store/config"
	"gitlab.com/hokanio/hokan/pkg/store/db"
	"gitlab.com/hokanio/hokan/pkg/store/directory"
	"gitlab.com/hokanio/hokan/pkg/store/file"
)

var storeSet = wire.NewSet(
	provideDatabase,
	provideDirectoryStore,
	provideFileStore,
	provideConfigStore,
)

func provideDatabase(config config.Config) (core.DB, error) {
	return db.Connect(config.Database.Path)
}

func provideDirectoryStore(db core.DB, config config.Config) core.DirectoryStore {
	dirs := directory.New(db, config.Backup.StorageName)
	return dirs
}

func provideFileStore(db core.DB) core.FileStore {
	files := file.New(db)
	return files
}

func provideConfigStore(db core.DB) core.ConfigStore {
	return configStore.New(db)
}
