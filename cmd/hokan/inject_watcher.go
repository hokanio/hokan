package main

import (
	"context"

	"github.com/google/wire"
	"gitlab.com/hokanio/hokan/cmd/hokan/config"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/watcher"

	"gitlab.com/hokanio/notify"
	notifywatcher "gitlab.com/hokanio/notify/watcher"
)

var watcherSet = wire.NewSet(
	provideWatcher,
	provideNotifier,
	provideWatcherConfig,
)

func provideWatcher(ctx context.Context,
	dirStore core.DirectoryStore,
	event core.EventCreator,
	notifier core.Notifier,
	sse core.ServerSideEventCreator,
	config core.WatcherConfig) (core.Watcher, error) {
	return watcher.New(ctx, dirStore, event, notifier, sse, config)
}

func provideNotifier(ctx context.Context) core.Notifier {
	return notify.Setup(ctx, &notifywatcher.Options{})
}

func provideWatcherConfig(config config.Config) core.WatcherConfig {
	return core.WatcherConfig{
		IgnoreFiles: config.Backup.IgnoreFiles,
	}
}
