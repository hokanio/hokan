package main

import (
	"bufio"
	"context"
	"os"

	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"gitlab.com/hokanio/hokan/cmd/hokan/config"
)

func main() {
	conf, err := config.Environ()
	if err != nil {
		logger := logrus.WithError(err)
		logger.Fatal("main: invalid configuration")
	}

	initLogger(conf.Logging)
	ctx := context.Background()

	app, err := InitializeApplication(ctx, conf)
	if err != nil {
		printFatal(err, conf)
	}

	printInfo(conf)

	g := errgroup.Group{}
	g.Go(func() error {
		logrus.WithFields(
			logrus.Fields{
				"port": conf.Server.Port,
			},
		).Info("main: starting the http server")
		return app.server.ListenAndServe(ctx)
	})
	g.Go(func() error {
		if config.IsWindows() {
			fixPowerShell()
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		logrus.Fatal("main: program terminated")
	}
}

func printFatal(err error, conf config.Config) {
	e := logrus.WithError(err)

	switch conf.Backup.StorageName {
	case config.LocalStorageName:
		e = e.WithField("storing backup to %q", conf.Backup.Local.Path)
	case config.MinioStorageName:
		e = e.WithField("bucket", conf.Backup.LocalMachineName)
	}

	e.WithField("storage-name", conf.Backup.StorageName).
		WithField("default", conf.Backup.DefaultConfig).
		WithField("db-path", conf.Database.Path).
		Fatal("can't start 'hokan.io' service")
}

func printInfo(conf config.Config) {
	logrus.
		WithField("storage-name", conf.Backup.StorageName).
		WithField("default", conf.Backup.DefaultConfig).
		WithField("db-path", conf.Database.Path).
		Info("starting 'hokan' service")

	switch conf.Backup.StorageName {
	case config.LocalStorageName:
		logrus.Printf("storing backup to %q", conf.Backup.Local.Path)
	case config.MinioStorageName:
		logrus.WithField("bucket-name", conf.Backup.LocalMachineName).
			Printf("minio API url: %s", conf.Backup.MinIO.Endpoint)
	}
}

func initLogger(conf config.Logging) {
	var logLevel = logrus.InfoLevel
	if conf.Debug {
		logLevel = logrus.DebugLevel
	}
	logrus.SetLevel(logLevel)
	logrus.SetOutput(colorable.NewColorableStdout())

	if conf.Text {
		logrus.SetFormatter(&logrus.TextFormatter{
			ForceColors:            conf.Color,
			DisableColors:          !conf.Color,
			FullTimestamp:          true,
			TimestampFormat:        "2006-01-02 15:04:05",
			DisableLevelTruncation: true,
			ForceQuote:             false,
		})
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{
			PrettyPrint: conf.Pretty,
		})
	}
}

func fixPowerShell() {
	reader := bufio.NewReader(os.Stdin)
	for {
		reader.ReadString('\n')
	}
}
