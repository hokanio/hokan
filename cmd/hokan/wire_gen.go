// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package main

import (
	"context"
	"gitlab.com/hokanio/hokan/cmd/hokan/config"
)

// Injectors from wire.go:

// for more info see: https://github.com/google/wire
func InitializeApplication(ctx context.Context, config2 config.Config) (application, error) {
	db, err := provideDatabase(config2)
	if err != nil {
		return application{}, err
	}
	fileStore := provideFileStore(db)
	directoryStore := provideDirectoryStore(db, config2)
	eventCreator := provideEventCreator()
	server := apiServerProvider(fileStore, directoryStore, eventCreator)
	configStore := provideConfigStore(db)
	backupOptions := provideBackupOptions(config2)
	webServer := provideWebHandler(configStore, directoryStore, backupOptions)
	v := proviceBackupResultChan()
	serverSideEventCreator := provideServerSideEventCreator(ctx, v)
	eventsServer := eventsServerProvider(serverSideEventCreator)
	mainHealthzHandler := provideHealthz()
	handler := provideRouter(server, webServer, eventsServer, mainHealthzHandler)
	serverServer := provideServer(handler, config2)
	notifier := provideNotifier(ctx)
	watcherConfig := provideWatcherConfig(config2)
	watcher, err := provideWatcher(ctx, directoryStore, eventCreator, notifier, serverSideEventCreator, watcherConfig)
	if err != nil {
		return application{}, err
	}
	backup, err := provideBackup(ctx, configStore, fileStore, eventCreator, v, backupOptions)
	if err != nil {
		return application{}, err
	}
	mainApplication := newApplication(serverServer, directoryStore, watcher, backup)
	return mainApplication, nil
}
