package backup

import (
	"context"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/backup/event"
	"gitlab.com/hokanio/hokan/pkg/backup/local"
	"gitlab.com/hokanio/hokan/pkg/backup/minio"
	"gitlab.com/hokanio/hokan/pkg/backup/void"
	"gitlab.com/hokanio/hokan/pkg/core"
)

var backupStorageRegister = map[string]core.BackupFactory{
	"void":  void.New,
	"local": local.New,
	"minio": minio.New,
}

func New(ctx context.Context,
	configStore core.ConfigStore,
	fileStore core.FileStore,
	events core.EventCreator,
	results chan core.BackupResult,
	options *core.BackupOptions) (core.Backup, error) {
	backup, err := getBackupStorage(options.Name)
	if err != nil {
		log.WithField("backup", options.Name).
			WithError(err).
			Fatal("can't initiate new backup storage")
		return nil, err
	}

	b, err := backup(ctx, configStore, fileStore, events, options)
	if err != nil {
		return nil, err
	}

	event.InitHandler(ctx, events, b, fileStore, results)

	return b, nil
}

func getBackupStorage(name string) (core.BackupFactory, error) {
	if _, ok := backupStorageRegister[name]; ok {
		return backupStorageRegister[name], nil
	}
	return nil, core.ErrBackupStorageNotFound
}
