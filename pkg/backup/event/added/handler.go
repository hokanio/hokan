package added

import (
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/backup/utils"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const errUnknown = "unexpected error from the storage"

type fileAdded struct {
	*core.EventHandler
}

func New(handler *core.EventHandler) core.EventProcessor {
	return &fileAdded{handler}
}

func (f *fileAdded) Name() string {
	return core.EventToString(core.FileAdded)
}

func (f *fileAdded) Process(e *core.EventData) error {
	file, ok := e.Data.(core.File)
	if !ok {
		return fmt.Errorf("invalid event data: %v", e.Data)
	}
	if e.Type != core.FileAdded {
		return fmt.Errorf("added.Process(): invalid event type: %v", core.EventToString(e.Type))
	}
	f.saveFileToBackup(&file)
	return nil
}

func (f *fileAdded) saveFileToBackup(file *core.File) {
	storedFile, err := f.FileStore.Find(f.Ctx, &core.FileSearchOptions{
		FilePath: file.Path,
	})
	// file is new and is not found in the backup
	if errors.Is(err, core.ErrFileNotFound) {
		f.Backup.Save(f.Ctx, f.Results, file, &core.BackupOperationOptions{})
		return
	}
	// something else is wrong
	if err != nil {
		log.WithError(err).
			WithFields(log.Fields{
				"file": file.Path,
			}).
			Error(errUnknown)
		f.Results <- core.BackupResult{
			Success: false,
			Message: errUnknown,
			Error:   err,
		}
		return
	}

	// file has been changed, but we got the wrong event.
	// we will send the file to the backup
	if utils.FileHasChanged(file, storedFile) {
		log.Printf("added.saveFileToBackup(): file %q has been changed since the last update", file.Path)
		f.Backup.Save(f.Ctx, f.Results, file, &core.BackupOperationOptions{})
		return
	}

	f.Results <- core.BackupResult{
		Success: true,
		Message: core.BackupNoChangeMessage,
	}
}
