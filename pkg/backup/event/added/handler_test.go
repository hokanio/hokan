package added

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const (
	filePath   = "test.txt"
	successMsg = "OK desu"
)

func TestNew(t *testing.T) {
	h := New(&core.EventHandler{})
	assert.Equal(t, "file added", h.Name())
}

func TestBadData(t *testing.T) {
	f := &fileAdded{}
	err := f.Process(&core.EventData{
		Type: core.FileAdded,
		Data: "file",
	})
	assert.Equal(t, "invalid event data: file", err.Error())

	err = f.Process(&core.EventData{
		Type: core.FileChanged,
		Data: core.File{},
	})
	assert.Equal(t, "added.Process(): invalid event type: file changed", err.Error())
}

func TestEventProcessing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	backup := mocks.NewMockBackup(controller)
	fileStore := mocks.NewMockFileStore(controller)
	result := make(chan core.BackupResult)
	file := core.File{
		Path: filePath,
	}

	event := &fileAdded{
		EventHandler: &core.EventHandler{
			Ctx:       context.Background(),
			Backup:    backup,
			Results:   result,
			FileStore: fileStore,
		},
	}

	t.Run("success: file not found", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(nil, core.ErrFileNotFound)

		backup.EXPECT().Save(context.Background(), result, &file, &core.BackupOperationOptions{})

		err := event.Process(&core.EventData{
			Type: core.FileAdded,
			Data: file,
		})
		assert.NoError(t, err)
	})

	t.Run("fail: error form file storage", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(nil, assert.AnError)

		go func() {
			err := event.Process(&core.EventData{
				Type: core.FileAdded,
				Data: file,
			})
			assert.NoError(t, err)
		}()

		msg := <-result
		assert.False(t, msg.Success)
	})

	t.Run("success: file added", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(&core.File{
			Path:     filePath,
			Checksum: "newCheckSum",
		}, nil)

		backup.EXPECT().Save(context.Background(), result, &file, &core.BackupOperationOptions{})

		err := event.Process(&core.EventData{
			Type: core.FileAdded,
			Data: file,
		})
		assert.NoError(t, err)
	})

	t.Run("success: file not changed", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(&core.File{
			Path: filePath,
		}, nil)

		go func() {
			err := event.Process(&core.EventData{
				Type: core.FileAdded,
				Data: file,
			})
			assert.NoError(t, err)
		}()

		msg := <-result
		assert.True(t, msg.Success)
		assert.Equal(t, core.BackupNoChangeMessage, msg.Message)
	})
}
