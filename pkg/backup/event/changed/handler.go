package changed

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/backup/utils"
	"gitlab.com/hokanio/hokan/pkg/core"
)

type fileChanged struct {
	*core.EventHandler
}

func New(handler *core.EventHandler) core.EventProcessor {
	return &fileChanged{handler}
}

func (f *fileChanged) Name() string {
	return core.EventToString(core.FileChanged)
}

func (f *fileChanged) Process(e *core.EventData) error {
	file, ok := e.Data.(core.File)
	if !ok {
		return fmt.Errorf("invalid event data: %v", e.Data)
	}
	if e.Type != core.FileChanged {
		return fmt.Errorf("changed.Process(): invalid event type: %v", core.EventToString(e.Type))
	}
	log.Printf("changed.Process(): for %q was fired ", file.Path)
	f.saveFileToBackup(&file)
	return nil
}

func (f *fileChanged) saveFileToBackup(file *core.File) {
	storedFile, err := f.FileStore.Find(f.Ctx, &core.FileSearchOptions{
		FilePath: file.Path,
	})
	if err != nil {
		log.WithError(err).
			WithFields(log.Fields{
				"file": file.Path,
			}).Error("Can't find file in the local storage")
		f.Results <- core.BackupResult{
			Success: false,
			Message: "Can't find file in the local storage",
			Error:   err,
		}
		return
	}
	if utils.FileHasChanged(file, storedFile) {
		f.Backup.Save(f.Ctx, f.Results, file, &core.BackupOperationOptions{})
		return
	}

	f.Results <- core.BackupResult{
		Success: true,
		Message: core.BackupNoChangeMessage,
	}
}
