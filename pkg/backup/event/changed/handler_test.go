package changed

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const (
	filePath   = "test.txt"
	successMsg = "OK desu"
	checksumA  = "123"
	checksumB  = "456"
)

func TestEventProcessing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	backup := mocks.NewMockBackup(controller)
	fileStore := mocks.NewMockFileStore(controller)
	result := make(chan core.BackupResult)
	file := core.File{
		Path:     filePath,
		Checksum: checksumA,
	}

	event := &fileChanged{
		EventHandler: &core.EventHandler{
			Ctx:       context.Background(),
			Backup:    backup,
			Results:   result,
			FileStore: fileStore,
		},
	}

	t.Run("fail: file find error", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(nil, assert.AnError)

		go func() {
			err := event.Process(&core.EventData{
				Type: core.FileChanged,
				Data: file,
			})
			assert.NoError(t, err)
		}()

		msg := <-result
		assert.False(t, msg.Success)
	})

	t.Run("success: file not changed", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(&core.File{
			Path:     filePath,
			Checksum: checksumA,
		}, nil)

		go func() {
			err := event.Process(&core.EventData{
				Type: core.FileChanged,
				Data: file,
			})
			assert.NoError(t, err)
		}()

		msg := <-result
		assert.True(t, msg.Success)
	})

	t.Run("success: file changed", func(t *testing.T) {
		fileStore.EXPECT().Find(context.Background(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(&core.File{
			Path:     filePath,
			Checksum: checksumB,
		}, nil)

		backup.EXPECT().Save(context.Background(), result, &file, &core.BackupOperationOptions{})

		err := event.Process(&core.EventData{
			Type: core.FileChanged,
			Data: file,
		})
		assert.NoError(t, err)
	})
}

func TestNew(t *testing.T) {
	h := New(&core.EventHandler{})
	assert.Equal(t, "file changed", h.Name())
}

func TestBadData(t *testing.T) {
	f := &fileChanged{}
	err := f.Process(&core.EventData{
		Type: core.FileChanged,
		Data: "foo",
	})
	assert.Equal(t, "invalid event data: foo", err.Error())

	err = f.Process(&core.EventData{
		Type: core.FileAdded,
		Data: core.File{},
	})
	assert.Equal(t, "changed.Process(): invalid event type: file added", err.Error())
}
