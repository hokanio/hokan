package event

import (
	"context"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/backup/event/added"
	"gitlab.com/hokanio/hokan/pkg/backup/event/changed"
	"gitlab.com/hokanio/hokan/pkg/backup/event/removed"
	"gitlab.com/hokanio/hokan/pkg/backup/event/renamed"
	"gitlab.com/hokanio/hokan/pkg/backup/event/rescan"
	"gitlab.com/hokanio/hokan/pkg/backup/event/restore"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const operationsPerSecond = 5

var eventPoolConfig = map[core.EventType]int{
	core.FileAdded:      5,
	core.FileChanged:    5,
	core.FileRemoved:    1,
	core.WatchDirRescan: 1,
	core.FileRenamed:    3,
	core.RestoreFile:    5,
}

var eventFactory = map[core.EventType]core.EventProcessrFactory{
	core.FileAdded:      added.New,
	core.FileChanged:    changed.New,
	core.FileRemoved:    removed.New,
	core.WatchDirRescan: rescan.New,
	core.FileRenamed:    renamed.New,
	core.RestoreFile:    restore.New,
}

func InitHandler(ctx context.Context,
	events core.EventCreator,
	backup core.Backup,
	fileStore core.FileStore,
	result chan core.BackupResult) {
	handler := &core.EventHandler{
		Ctx:       ctx,
		Events:    events,
		Backup:    backup,
		FileStore: fileStore,
		Results:   result,
	}

	for eventType, factory := range eventFactory {
		processor := factory(handler)
		event := events.Subscribe(ctx, eventType)
		for i := 0; i < eventPoolConfig[eventType]; i++ {
			go listener(ctx, event, processor)
		}
	}
}

func listener(ctx context.Context, event <-chan *core.EventData, processor core.EventProcessor) {
	// log.Infof("starting listener for event type [%s]", processor.Name())
	for {
		select {
		case <-ctx.Done():
			log.Info("event.Listener(): event stream canceled")
			return
		case e := <-event:
			err := processor.Process(e)
			if err != nil {
				log.WithError(err).
					WithField("event", core.EventToString(e.Type)).
					Error("event.Listener(): can't process event")
			}
		}
	}
}
