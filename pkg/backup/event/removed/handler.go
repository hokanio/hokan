package removed

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/core"
)

type fileRemoved struct {
	*core.EventHandler
}

func New(handler *core.EventHandler) core.EventProcessor {
	return &fileRemoved{handler}
}

func (f *fileRemoved) Name() string {
	return core.EventToString(core.FileRemoved)
}

func (f *fileRemoved) Process(e *core.EventData) error {
	file, ok := e.Data.(core.File)
	if !ok {
		return fmt.Errorf("invalid event data")
	}
	log.Printf("removed.Process(): file %q is deleted from backup", file.Path)
	err := f.FileStore.Delete(f.Ctx, &file)
	if err != nil {
		log.Printf("removed.Process(): Delete: %v", err)
		f.Results <- core.BackupResult{
			Success: false,
			Error:   err,
		}
		return err
	}

	f.Results <- core.BackupResult{
		Success: true,
		Message: core.BackupFileDeletedMessage,
	}

	return nil
}
