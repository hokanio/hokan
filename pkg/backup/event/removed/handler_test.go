package removed

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const filePath = "file/test.txt"

func Test_fileRemoved_Name(t *testing.T) {
	f := New(&core.EventHandler{})
	name := f.Name()
	assert.Equal(t, "file removed", name)
}

func TestBadData(t *testing.T) {
	f := &fileRemoved{}
	err := f.Process(&core.EventData{
		Type: core.FileRemoved,
		Data: "foo",
	})
	assert.Equal(t, "invalid event data", err.Error())
}

func TestEventProcessing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	backup := mocks.NewMockBackup(controller)
	fileStore := mocks.NewMockFileStore(controller)
	result := make(chan core.BackupResult)
	file := core.File{
		Path: filePath,
	}

	fileRemoved := &fileRemoved{
		EventHandler: &core.EventHandler{
			Ctx:       context.TODO(),
			Backup:    backup,
			Results:   result,
			FileStore: fileStore,
		},
	}

	t.Run("fail: backup delete error", func(t *testing.T) {
		fileStore.EXPECT().Delete(context.Background(), &core.File{
			Path: filePath,
		}).Return(assert.AnError)

		go func() {
			err := fileRemoved.Process(&core.EventData{
				Type: core.FileRemoved,
				Data: file,
			})
			assert.Error(t, err)
		}()

		msg := <-result
		assert.False(t, msg.Success)
	})

	t.Run("success", func(t *testing.T) {
		fileStore.EXPECT().Delete(context.Background(), &core.File{
			Path: filePath,
		}).Return(nil)

		go func() {
			err := fileRemoved.Process(&core.EventData{
				Type: core.FileRemoved,
				Data: file,
			})
			assert.NoError(t, err)
		}()

		msg := <-result
		assert.True(t, msg.Success)
	})
}
