package renamed

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/core"
)

type fileRenamed struct {
	*core.EventHandler
}

func New(handler *core.EventHandler) core.EventProcessor {
	return &fileRenamed{handler}
}

func (f *fileRenamed) Name() string {
	return core.EventToString(core.FileRenamed)
}

func (f *fileRenamed) Process(e *core.EventData) error {
	file, ok := e.Data.(core.File)
	if !ok {
		return fmt.Errorf("invalid event data")
	}
	log.Printf("renamed.Process(): file %q is renamed to %q", file.OldPath, file.Path)

	// TODO: do we need to track this event in the storage or backup?

	f.Results <- core.BackupResult{
		Success: true,
		Message: core.BackupFileRenamedMessage,
	}
	return nil
}
