package renamed

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const (
	filePath    = "file/test.txt"
	oldFilePath = "file/old.txt"
)

func Test_fileRemoved_Name(t *testing.T) {
	f := New(&core.EventHandler{})
	name := f.Name()
	assert.Equal(t, "file renamed", name)
}

func TestEventProcessing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	backup := mocks.NewMockBackup(controller)
	fileStore := mocks.NewMockFileStore(controller)
	result := make(chan core.BackupResult)
	file := core.File{
		Path:    filePath,
		OldPath: oldFilePath,
	}

	fileRenamed := &fileRenamed{
		EventHandler: &core.EventHandler{
			Ctx:       context.Background(),
			Backup:    backup,
			Results:   result,
			FileStore: fileStore,
		},
	}

	t.Run("fail: bad event data", func(t *testing.T) {
		err := fileRenamed.Process(&core.EventData{
			Type: core.FileRenamed,
			Data: "foo",
		})
		assert.Equal(t, "invalid event data", err.Error())
	})

	t.Run("success", func(t *testing.T) {
		go func() {
			err := fileRenamed.Process(&core.EventData{
				Type: core.FileRenamed,
				Data: file,
			})
			assert.NoError(t, err)
		}()
		msg := <-result
		assert.True(t, msg.Success)
		assert.Equal(t, "file was renamed", msg.Message)
	})
}
