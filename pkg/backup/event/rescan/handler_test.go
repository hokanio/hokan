package rescan

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/pkg/core"
)

func TestEventProcessing(t *testing.T) {
	f := New(&core.EventHandler{})
	name := f.Name()
	assert.Equal(t, "dir rescan", name)

	t.Run("success", func(t *testing.T) {
		err := f.Process(&core.EventData{
			Type: core.WatchDirRescan,
		})
		assert.NoError(t, err)
	})
}
