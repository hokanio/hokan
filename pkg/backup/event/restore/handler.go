package restore

import (
	"fmt"

	"gitlab.com/hokanio/hokan/pkg/core"
)

type restoreFile struct {
	*core.EventHandler
}

func New(handler *core.EventHandler) core.EventProcessor {
	return &restoreFile{handler}
}

func (f *restoreFile) Name() string {
	return core.EventToString(core.RestoreFile)
}

func (f *restoreFile) Process(e *core.EventData) error {
	file, ok := e.Data.(core.File)
	if !ok {
		return fmt.Errorf("invalid event data")
	}
	if e.Type != core.RestoreFile {
		return fmt.Errorf("invalid event type")
	}

	storedFile, err := f.FileStore.Find(f.Ctx, &core.FileSearchOptions{
		FilePath: file.Path,
	})
	if err != nil {
		f.Results <- core.BackupResult{
			Success: false,
			Error:   err,
		}
		return err
	}

	f.Backup.Restore(f.Ctx, f.Results, []*core.File{storedFile}, &core.BackupOperationOptions{})

	return nil
}
