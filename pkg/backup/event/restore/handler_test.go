package restore

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const (
	fileID   = "123"
	filePath = "file/test.txt"
)

func TestEventProcessing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	backup := mocks.NewMockBackup(controller)
	fileStore := mocks.NewMockFileStore(controller)
	result := make(chan core.BackupResult)
	file := &core.File{Path: filePath}

	f := New(&core.EventHandler{
		Ctx:       context.TODO(),
		Backup:    backup,
		Results:   result,
		FileStore: fileStore,
	})
	name := f.Name()
	assert.Equal(t, "restore file", name)

	t.Run("fail: bad event data", func(t *testing.T) {
		err := f.Process(&core.EventData{
			Type: core.RestoreFile,
			Data: "foo",
		})
		assert.Equal(t, "invalid event data", err.Error())
	})

	t.Run("fail: bad event type", func(t *testing.T) {
		err := f.Process(&core.EventData{
			Type: core.FileAdded,
			Data: *file,
		})
		assert.Equal(t, "invalid event type", err.Error())
	})

	t.Run("fail: error from the file storage", func(t *testing.T) {
		fileStore.EXPECT().Find(context.TODO(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(nil, assert.AnError)

		go func() {
			err := f.Process(&core.EventData{
				Type: core.RestoreFile,
				Data: *file,
			})
			assert.Error(t, err)
		}()
		msg := <-result
		assert.False(t, msg.Success)
	})

	t.Run("success", func(t *testing.T) {
		expectedFile := &core.File{
			Path: filePath,
			ID:   fileID,
		}

		fileStore.EXPECT().Find(context.TODO(), &core.FileSearchOptions{
			FilePath: filePath,
		}).Return(expectedFile, nil)

		backup.EXPECT().Restore(context.TODO(), result, []*core.File{expectedFile}, &core.BackupOperationOptions{}).
			DoAndReturn(func(ctx context.Context, result chan core.BackupResult, files []*core.File, opt *core.BackupOperationOptions) {
				assert.Equal(t, fileID, files[0].ID)
			})

		err := f.Process(&core.EventData{
			Type: core.RestoreFile,
			Data: *file,
		})
		assert.NoError(t, err)
	})
}
