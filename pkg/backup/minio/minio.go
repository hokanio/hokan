package minio

import (
	"context"
	"fmt"
	"os"
	"path"
	"regexp"

	"github.com/minio/minio-go"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/core"
)

var bucketNameRegexp = regexp.MustCompile("^[a-zA-Z0-9_.]+$")

const (
	name = "minio"
)

type minioStore struct {
	client      core.MinioWrapper
	fileStore   core.FileStore
	configStore core.ConfigStore
	event       core.EventCreator
	bucketName  string
}

func New(ctx context.Context,
	configStore core.ConfigStore,
	fs core.FileStore,
	event core.EventCreator,
	conf *core.BackupOptions) (core.Backup, error) {
	log.WithFields(log.Fields{
		"backup": conf.Name,
	}).Info("Starting new backup storage")

	minioClient, err := NewMinioWrapper(configStore, &core.MinioConfig{
		Endpoint:        conf.TargetURL,
		AccessKeyID:     conf.AccessKeyID,
		SecretAccessKey: conf.SecretAccessKey,
		UseSSL:          conf.UseSSL,
		Bucket:          conf.LocalMachineName,
	})
	if err != nil {
		return nil, err
	}

	return &minioStore{
		client:     minioClient,
		fileStore:  fs,
		event:      event,
		bucketName: conf.LocalMachineName,
	}, nil
}

func (s *minioStore) Name() string {
	return name
}

func (s *minioStore) Save(ctx context.Context, result chan core.BackupResult, file *core.File, opt *core.BackupOperationOptions) {
	logger := log.WithFields(log.Fields{
		"backup": name,
		"file":   file.Path,
	})

	objectName := path.Clean(file.Path)
	options := minio.PutObjectOptions{
		UserMetadata: map[string]string{
			"path":      file.Path,
			"size":      fmt.Sprintf("%d", file.Info.Size),
			"name":      file.Info.Name,
			"mode-time": file.Info.ModTime.String(),
			"checksum":  file.Checksum,
		},
		// TODO: we can use Progress for the reporting the progress back to the client
	}

	r, err := os.Open(file.Path)
	if err != nil {
		result <- core.BackupResult{
			Success: false,
			Error:   err,
			Message: fmt.Sprintf("can't open file %q", file.Path),
		}
		return
	}
	defer r.Close()
	n, err := s.client.PutObjectWithContext(ctx, s.bucketName, objectName, r, file.Info.Size, options)
	if err != nil {
		result <- core.BackupResult{
			Success: false,
			Error:   err,
			Message: fmt.Sprintf("can't save file %q", file.Path),
		}
		return
	}
	logger.Infof("Successfully uploaded %s of size %d", objectName, n)
	saveErr := s.fileStore.Save(ctx, file)
	if saveErr != nil {
		result <- core.BackupResult{
			Success: false,
			Error:   saveErr,
			Message: fmt.Sprintf("can't save backup info for the file %q", file.Path),
		}
		return
	}

	result <- core.BackupResult{
		Success: true,
		Message: core.BackupSuccessMessage,
	}
}

func (s *minioStore) Restore(ctx context.Context, result chan core.BackupResult, files []*core.File, opt *core.BackupOperationOptions) {
	for _, file := range files {
		objectName := path.Clean(file.Path)
		err := s.client.FGetObjectWithContext(ctx, s.bucketName, objectName, file.Path, minio.GetObjectOptions{})
		if err != nil {
			result <- core.BackupResult{
				Success: false,
				Error:   err,
				Message: fmt.Sprintf("can't restore file %q from %q", file.Path, s.bucketName),
			}
			return
		}

		// after the file was restored from the backup, we reset `IsDeleted`` status to `false`
		file.IsDeleted = false
		if err := s.fileStore.Save(ctx, file); err != nil {
			result <- core.BackupResult{
				Success: true,
				Error:   err,
				Message: fmt.Sprintf("file %q was restored but status was not updated", file.Path),
			}
			return
		}

		result <- core.BackupResult{
			Success: true,
			Message: core.BuckupFileRestoredMessage,
		}
	}
}

func (s *minioStore) Delete(ctx context.Context, result chan core.BackupResult, files []*core.File, opt *core.BackupOperationOptions) {
	log.WithField("backup", name).Print("Delete")
	result <- core.BackupResult{
		Success: false,
		Error:   core.ErrNotImplemented,
	}
}

func (s *minioStore) Ping(ctx context.Context) error {
	log.WithField("backup", name).Print("target")
	_, err := s.client.BucketExists(s.bucketName)
	return err
}
