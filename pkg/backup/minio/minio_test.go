package minio

import (
	"context"
	"os"
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/watcher/utils"
)

var testFilePath = "testdata/test.txt"
var testBucket = "hokan.io"

func getTestingFile(t *testing.T) string {
	pwd, err := os.Getwd()
	assert.NoError(t, err)
	return filepath.Join(pwd, testFilePath)
}

func Test_minioStore_SaveNewFile(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	checksum, info, err := utils.FileChecksumInfo(getTestingFile(t))
	assert.NoError(t, err)

	file := &core.File{
		Path:     testFilePath,
		Checksum: checksum,
		Info:     info,
	}

	fileStore := mocks.NewMockFileStore(controller)
	fileStore.EXPECT().Save(context.TODO(), file).Return(nil)

	minioClient := mocks.NewMockMinioWrapper(controller)
	minioClient.EXPECT().
		PutObjectWithContext(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), info.Size, gomock.Any()).
		Return(int64(11), nil)

	store := &minioStore{
		fileStore: fileStore,
		client:    minioClient,
	}

	result := make(chan core.BackupResult)
	go func() {
		store.Save(context.Background(), result, file, nil)
	}()
	data := <-result

	assert.Equal(t, core.BackupResult{
		Success: true,
		Error:   nil,
		Message: "requested operation was successful",
	}, data)
}

func Test_minioStore_SaveError(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	checksum, info, err := utils.FileChecksumInfo(getTestingFile(t))
	assert.NoError(t, err)
	result := make(chan core.BackupResult)

	file := &core.File{
		Path:     testFilePath,
		Checksum: checksum,
		Info:     info,
	}

	fileStore := mocks.NewMockFileStore(controller)
	minioClient := mocks.NewMockMinioWrapper(controller)
	minioClient.EXPECT().
		PutObjectWithContext(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), info.Size, gomock.Any()).
		Return(int64(0), assert.AnError)

	store := &minioStore{
		fileStore: fileStore,
		client:    minioClient,
	}

	go func() {
		store.Save(context.Background(), result, file, nil)
	}()
	data := <-result

	assert.Equal(t, core.BackupResult{
		Success: false,
		Error:   assert.AnError,
		Message: `can't save file "testdata/test.txt"`,
	}, data)
}

func Test_minioStore_SaveFileChange(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	_, info, err := utils.FileChecksumInfo(getTestingFile(t))
	assert.NoError(t, err)

	fileB := &core.File{
		Path:     testFilePath,
		Checksum: "abX",
		Info:     info,
	}

	fileStore := mocks.NewMockFileStore(controller)
	fileStore.EXPECT().Save(context.TODO(), fileB).Return(nil)

	minioClient := mocks.NewMockMinioWrapper(controller)
	minioClient.EXPECT().
		PutObjectWithContext(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), info.Size, gomock.Any()).
		Return(int64(11), nil)

	store := &minioStore{
		fileStore: fileStore,
		client:    minioClient,
	}

	result := make(chan core.BackupResult)
	go store.Save(context.TODO(), result, fileB, nil)
	data := <-result

	assert.Equal(t, core.BackupResult{
		Success: true,
		Error:   nil,
		Message: "requested operation was successful",
	}, data)
}

func TestPing(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	fileStore := mocks.NewMockFileStore(controller)
	minioClient := mocks.NewMockMinioWrapper(controller)
	minioClient.EXPECT().BucketExists(gomock.Any()).Return(true, nil)
	store := &minioStore{
		fileStore: fileStore,
		client:    minioClient,
	}

	err := store.Ping(context.TODO())
	assert.NoError(t, err)
}

func Test_minioStore_Delete(t *testing.T) {
	store := &minioStore{}
	result := make(chan core.BackupResult)
	go store.Delete(context.TODO(), result, []*core.File{}, &core.BackupOperationOptions{})
	data := <-result
	assert.Equal(t, core.BackupResult{
		Success: false,
		Error:   core.ErrNotImplemented,
	}, data)
}

func Test_minioStore_Restore(t *testing.T) {
	t.Skip()
	store := &minioStore{}
	result := make(chan core.BackupResult)
	go store.Restore(context.TODO(), result, []*core.File{}, &core.BackupOperationOptions{})
	data := <-result
	assert.Equal(t, core.BackupResult{
		Success: false,
		Error:   core.ErrNotImplemented,
	}, data)
}
