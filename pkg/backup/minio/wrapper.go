package minio

import (
	"context"
	"io"

	"github.com/fujiwara/shapeio"
	"github.com/minio/minio-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/hokanio/hokan/pkg/core"
)

const (
	defultLocation = ""
	fileReadLimit  = 10 * 1024 * 1024 // 10MB/sec
)

type wrapper struct {
	client *minio.Client
}

func NewMinioWrapper(configStore core.ConfigStore, config *core.MinioConfig) (core.MinioWrapper, error) {
	minioClient, err := minio.New(config.Endpoint, config.AccessKeyID, config.SecretAccessKey, config.UseSSL)
	if err != nil {
		log.WithError(err).Error("Can't create new minio client")
		return nil, err
	}

	cli := &wrapper{
		client: minioClient,
	}

	storedConf, err := configStore.Get(context.Background(), name)
	if err != nil {
		if err == core.ErrConfigNotFound {
			storedConf = &core.Config{
				StorageName: name,
				BucketName:  config.Bucket,
			}
			if errCreate := configStore.Create(context.Background(), storedConf); errCreate != nil {
				return nil, errCreate
			}
		} else {
			return nil, err
		}
	}

	return cli, cli.createNewBucket(config.Bucket)
}

func (w *wrapper) createNewBucket(name string) error {
	// Check to see if we already own this bucket
	exists, err := w.client.BucketExists(name)
	if err != nil {
		return err
	}
	if err == nil && exists {
		return nil
	}

	if err := w.client.MakeBucket(name, defultLocation); err != nil {
		return err
	}

	log.Printf("minio: Successfully created bucket %q\n", name)
	return nil
}

func (w *wrapper) FPutObjectWithContext(ctx context.Context, bucketName, objectName, filePath string, opts minio.PutObjectOptions) (int64, error) {
	return w.client.FPutObjectWithContext(ctx, bucketName, objectName, filePath, opts)
}

func (w *wrapper) BucketExists(bucketName string) (bool, error) {
	return w.client.BucketExists(bucketName)
}

func (w *wrapper) FGetObjectWithContext(ctx context.Context, bucketName, objectName, filePath string, opts minio.GetObjectOptions) error {
	return w.client.FGetObjectWithContext(ctx, bucketName, objectName, filePath, opts)
}

// PutObjectWithContext - Identical to PutObject call, but accepts context to facilitate request cancellation.
func (w *wrapper) PutObjectWithContext(ctx context.Context, bucketName, objectName string, r io.Reader, objectSize int64, opts minio.PutObjectOptions) (int64, error) {
	reader := shapeio.NewReaderWithContext(r, ctx)
	reader.SetRateLimit(fileReadLimit)
	return w.client.PutObjectWithContext(ctx, bucketName, objectName, reader, objectSize, opts)
}
