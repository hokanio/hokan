package utils

import (
	"gitlab.com/hokanio/hokan/pkg/core"
)

func FileHasChanged(newFile, storedFile *core.File) bool {
	if storedFile == nil {
		return true
	}
	if newFile.Path != storedFile.Path {
		return true
	}
	if newFile.Checksum != storedFile.Checksum {
		return true
	}
	return false
}

func sizeHasChanged(newFile, storedFile *core.File) bool {
	return newFile.Info.Size != storedFile.Info.Size
}
