package core

import (
	"context"
	"errors"
)

var ErrConfigNotFound = errors.New("config not found")

type Config struct {
	StorageName string `json:"storage_name"`
	BucketName  string `json:"bucket_name"`
}

type ConfigStore interface {
	Get(ctx context.Context, name string) (*Config, error)
	Create(ctx context.Context, conf *Config) error
	Update(ctx context.Context, conf *Config) error
	Delete(ctx context.Context, conf *Config) error
}
