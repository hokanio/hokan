package core

import (
	"context"
	"errors"
)

var (
	ErrDirectoryNotFound  = errors.New("directory not found")
	ErrEmptySearchOptions = errors.New("empty search options")
)

type DirectorySearchOptions struct {
	ID   string
	Path string
}

type Directory struct {
	ID          string   `json:"id"`
	Path        string   `json:"path"`
	Recursive   bool     `json:"recursive"`
	Machine     string   `json:"machine"`
	StorageName string   `json:"storage_name"`
	IgnoreFiles []string `json:"ignore"`
}

type DirectoryStats struct {
	InBackup            bool   `json:"in_backup"`
	Path                string `json:"path"`
	OS                  string `json:"os"`
	TotalFiles          int    `json:"total_files"`
	TotalSubDirectories int    `json:"total_dirs"`
	TotalSize           uint64 `json:"total_size"`
	TotalSizeH          string `json:"total_size_h"`
}

type DirectoryStore interface {
	List(ctx context.Context) ([]Directory, error)
	Find(ctx context.Context, options *DirectorySearchOptions) (*Directory, error)
	Create(ctx context.Context, dir *Directory) error
	Delete(ctx context.Context, dir *Directory) error
}
