package core

import (
	"context"
	"errors"
	"time"
)

var ErrFileNotFound = errors.New("file not found")
var ErrInvalidFileID = errors.New("invalid file id")

type FileInfo struct {
	Name    string    `json:"name"`
	Size    int64     `json:"size"`
	ModTime time.Time `json:"mod-time"`
}

type File struct {
	ID   string `json:"id"`
	Path string `json:"path"`
	// if file was renamed
	OldPath   string   `json:"old_path"`
	Checksum  string   `json:"checksum"`
	Info      FileInfo `json:"info"`
	IsDeleted bool     `json:"is_deleted"`
}

type FileListOptions struct {
	Query  string
	Offset uint64
	Limit  uint64
}

type FileSearchOptions struct {
	ID       string
	FilePath string
}

type FileStore interface {
	List(ctx context.Context, options *FileListOptions) ([]File, error)
	Find(ctx context.Context, options *FileSearchOptions) (*File, error)
	Save(ctx context.Context, file *File) error
	Delete(ctx context.Context, file *File) error
}
