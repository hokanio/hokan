package core

import (
	"context"
	"io"

	"github.com/minio/minio-go"
)

type MinioConfig struct {
	Endpoint        string
	AccessKeyID     string
	SecretAccessKey string
	Bucket          string
	UseSSL          bool
	RateLimit       int
}

type MinioWrapper interface {
	// Returns a stream of the object data. Most of the common errors occur when reading the stream.
	// -> minio.Object represents object reader. It implements io.Reader, io.Seeker, io.ReaderAt and io.Closer interfaces.
	// GetObject(ctx context.Context, bucketName, objectName string, opts GetObjectOptions) (*minio.Object, error)
	FPutObjectWithContext(ctx context.Context, bucketName, objectName, filePath string, opts minio.PutObjectOptions) (int64, error)
	PutObjectWithContext(ctx context.Context, bucketName, objectName string, r io.Reader, objectSize int64, opts minio.PutObjectOptions) (int64, error)
	FGetObjectWithContext(ctx context.Context, bucketName, objectName, filePath string, opts minio.GetObjectOptions) error
	BucketExists(bucketName string) (bool, error)
}
