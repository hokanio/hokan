package core

import (
	notifycore "gitlab.com/hokanio/notify/core"
	notifyevent "gitlab.com/hokanio/notify/event"
)

type WatchOptions struct {
	Rescan bool
}

// Notifier is an interface for start/stop watching directories for changes
type Notifier interface {
	Event() chan notifyevent.Event
	Log() chan notifyevent.Log
	RescanAll()
	StopWatching(string)
	StartWatching(string, *notifycore.WatchingOptions)
}
