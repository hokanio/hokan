package core

type WatcherConfig struct {
	IgnoreFiles []string
}

type Watcher interface {
	StartDirWatcher()
	StartFileWatcher()
	GetDirsToWatch() error
}
