package directory

import (
	home "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/core"
)

type Home struct {
	Path string
}

func NewHome() (*Home, error) {
	homeDir, err := home.Dir()
	if err != nil {
		log.WithError(err).Error("can't get hom dir")
		return nil, err
	}
	return &Home{
		Path: homeDir,
	}, nil
}

func (h *Home) UserFolders() []core.DirectoryStats {
	folders := []core.DirectoryStats{}
	for _, path := range h.KnownFolderids() {
		stats, err := Stats(path)
		if err != nil {
			log.WithError(err).
				Errorf("Can't get stats for path %q", path)
			continue
		}
		folders = append(folders, *stats)
	}
	return folders
}
