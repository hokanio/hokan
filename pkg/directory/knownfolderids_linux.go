// +build linux

package directory

import (
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

func (h *Home) KnownFolderids() map[string]string {
	folders := make(map[string]string)

	homeDir := h.Path
	pictures := filepath.Join(homeDir, "Pictures")
	if _, err := os.Stat(pictures); os.IsNotExist(err) {
		log.Infof("Pictures path not exist: %q", pictures)
	} else {
		folders["pictures"] = pictures
	}

	documents := filepath.Join(homeDir, "Documents")
	if _, err := os.Stat(pictures); os.IsNotExist(err) {
		log.Infof("Documents path not exist: %q", documents)
	} else {
		folders["documents"] = documents
	}

	desktop := filepath.Join(homeDir, "Desktop")
	if _, err := os.Stat(pictures); os.IsNotExist(err) {
		log.Infof("Desktop path not exist: %q", desktop)
	} else {
		folders["desktop"] = desktop
	}

	if len(folders) == 0 {
		folders["home"] = homeDir
	}

	return folders
}
