// +build linux

package directory

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var expectedFoldersAll = map[string]string{
	"desktop":   "testdata/home/Desktop",
	"documents": "testdata/home/Documents",
	"pictures":  "testdata/home/Pictures",
}

var expectedFoldersHome = map[string]string{
	"home": "testdata/",
}

func TestKnownFolderidsAll(t *testing.T) {
	h := &Home{
		Path: "testdata/home",
	}
	folders := h.KnownFolderids()
	assert.Equal(t, expectedFoldersAll, folders)
}

func TestKnownFolderidsHome(t *testing.T) {
	h := &Home{
		Path: "testdata/",
	}
	folders := h.KnownFolderids()
	assert.Equal(t, expectedFoldersHome, folders)
}
