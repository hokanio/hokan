// +build windows

package directory

import (
	log "github.com/sirupsen/logrus"

	"golang.org/x/sys/windows"
)

func (h *Home) KnownFolderids() map[string]string {
	folders := make(map[string]string)

	desktop, err := windows.KnownFolderPath(windows.FOLDERID_Desktop, windows.KF_FLAG_DEFAULT)
	if err != nil {
		log.WithError(err).
			Errorf("Can't get KnownFolderPath for Desktop")
	} else {
		folders["desktop"] = desktop
	}

	pictures, err := windows.KnownFolderPath(windows.FOLDERID_Pictures, windows.KF_FLAG_DEFAULT)
	if err != nil {
		log.WithError(err).
			Errorf("Can't get KnownFolderPath for Pictures")
	} else {
		folders["pictures"] = pictures
	}

	music, err := windows.KnownFolderPath(windows.FOLDERID_Music, windows.KF_FLAG_DEFAULT)
	if err != nil {
		log.WithError(err).
			Errorf("Can't get KnownFolderPath for Music")
	} else {
		folders["music"] = music
	}

	videos, err := windows.KnownFolderPath(windows.FOLDERID_Videos, windows.KF_FLAG_DEFAULT)
	if err != nil {
		log.WithError(err).
			Errorf("Can't get KnownFolderPath for Videos")
	} else {
		folders["videos"] = videos
	}

	documents, err := windows.KnownFolderPath(windows.FOLDERID_Documents, windows.KF_FLAG_DEFAULT)
	if err != nil {
		log.WithError(err).
			Errorf("Can't get KnownFolderPath for Documents")
	} else {
		folders["documents"] = documents
	}

	if len(folders) == 0 {
		folders["home"] = h.Path
	}

	return folders
}
