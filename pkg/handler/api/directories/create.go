package directories

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
	"gitlab.com/hokanio/hokan/pkg/logger"
)

const addDirErrMsg = "can't add a new directory to backup"

func HandleCreate(dirStore core.DirectoryStore, event core.EventCreator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logger.FromRequest(r)

		dirs := []core.Directory{}
		if err := json.NewDecoder(r.Body).Decode(&dirs); err != nil {
			handler.RespondWithStatusBadRequest(w, r,
				fmt.Errorf("invalid request body: %w", err))
			return
		}

		l.Debugf("adding dirs: %+v to the backup\n", dirs)

		for i := range dirs {
			dir := dirs[i]
			dir.ID = ""
			if err := dirStore.Create(r.Context(), &dir); err != nil {
				if errors.Is(err, core.ErrDirectoryNotFound) {
					handler.RespondWithStatusBadRequest(w, r,
						fmt.Errorf("can't find directory %q", dir.Path))
				} else {
					handler.RespondWithStatusInternalServerError(w, r,
						fmt.Errorf("%s: %w", addDirErrMsg, err))
				}
				return
			}

			err := event.Publish(r.Context(), &core.EventData{
				Type: core.WatchDirStart,
				Data: dir,
			})
			if err != nil {
				handler.RespondWithStatusInternalServerError(w, r,
					fmt.Errorf("%s: %w", addDirErrMsg, err))
				return
			}
		}

		handler.RespondWithStatusCreated(w, r, dirs)
	}
}
