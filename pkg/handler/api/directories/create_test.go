package directories_test

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler/api"
	"gitlab.com/hokanio/hokan/pkg/testing/tools"
)

var testFotosPath string = "C:\\Documents\\Fotos"

func Test_Create(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	dirStore := mocks.NewMockDirectoryStore(controller)
	eventCreator := mocks.NewMockEventCreator(controller)
	fileStore := mocks.NewMockFileStore(controller)

	server := api.New(fileStore, dirStore, eventCreator)
	testDirOK := core.Directory{Path: testFotosPath, Machine: "test", Recursive: true}
	testDirWrongPath := core.Directory{Path: "some/wrong/path", Machine: "test", Recursive: true}
	in := new(bytes.Buffer)

	t.Run("fail: bad request", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/directories", in)

		server.Handler().ServeHTTP(w, r)

		assert.Equal(t, http.StatusBadRequest, w.Code)
		assert.JSONEq(t, `{
			"code": 400,
			"message": "invalid request body: EOF",
			"status": "Bad Request"
		}`, w.Body.String())
	})

	t.Run("fail: error from local storage", func(t *testing.T) {
		dirs := &[]core.Directory{testDirOK}
		dirStore.EXPECT().Create(gomock.Any(), &testDirOK).
			Return(assert.AnError)

		err := json.NewEncoder(in).Encode(dirs)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/directories", in)

		server.Handler().ServeHTTP(w, r)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.JSONEq(t, `{
			"code": 500,
			"message": "can't add a new directory to backup: assert.AnError general error for testing",
			"status": "Internal Server Error"
		}`, w.Body.String())
	})

	t.Run("fail: dir not found", func(t *testing.T) {
		dirs := &[]core.Directory{testDirWrongPath}
		dirStore.EXPECT().Create(gomock.Any(), &testDirWrongPath).
			Return(core.ErrDirectoryNotFound)

		err := json.NewEncoder(in).Encode(dirs)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/directories", in)

		server.Handler().ServeHTTP(w, r)

		assert.Equal(t, http.StatusBadRequest, w.Code)
		assert.JSONEq(t, `{
			"code": 400,
			"message": "can't find directory \"some/wrong/path\"",
			"status": "Bad Request"
		}`, w.Body.String())
	})

	t.Run("fail: error from event creator", func(t *testing.T) {
		dirs := &[]core.Directory{testDirOK}

		dirStore.EXPECT().Create(gomock.Any(), &testDirOK).
			Return(nil)

		eventCreator.EXPECT().Publish(gomock.Any(), &core.EventData{
			Type: core.WatchDirStart,
			Data: testDirOK,
		}).Return(assert.AnError)

		err := json.NewEncoder(in).Encode(dirs)
		assert.NoError(t, err)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/directories", in)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.JSONEq(t, `{
			"code": 500,
			"message": "can't add a new directory to backup: assert.AnError general error for testing",
			"status": "Internal Server Error"
		}`, w.Body.String())
	})

	t.Run("success", func(t *testing.T) {
		dirStore.EXPECT().Create(gomock.Any(), gomock.Any()).
			Do(func(_ context.Context, dir *core.Directory) error {
				assert.Equal(t, testFotosPath, dir.Path)
				return nil
			})

		eventCreator.EXPECT().Publish(gomock.Any(), gomock.Any()).
			Do(func(_ context.Context, event *core.EventData) {
				assert.Equal(t, core.WatchDirStart, event.Type)
				assert.Equal(t, testFotosPath, event.Data.(core.Directory).Path)
			})

		dirs := &[]core.Directory{testDirOK}
		err := json.NewEncoder(in).Encode(dirs)
		assert.NoError(t, err)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/directories", in)

		server.Handler().ServeHTTP(w, r)
		body := strings.TrimSpace(w.Body.String())

		assert.Equal(t, http.StatusCreated, w.Code)
		tools.TestJSONPath(t, testFotosPath, "0.path", body)
		tools.TestJSONPath(t, "true", "0.recursive", body)
	})
}
