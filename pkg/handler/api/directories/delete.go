package directories

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
	"gitlab.com/hokanio/hokan/pkg/logger"
)

const removeDirErrMsg = "can't remove a directory from backup"

// HandleDelete creates an HTTP handle for DELETE operation.
func HandleDelete(dirStore core.DirectoryStore, event core.EventCreator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logger.FromRequest(r)
		l.Infof("%s %q", r.Method, r.URL.Path)

		pathID := chi.URLParam(r, "pathID")
		dir, err := dirStore.Find(r.Context(), &core.DirectorySearchOptions{ID: pathID})
		if errors.Is(err, core.ErrDirectoryNotFound) {
			handler.RespondWithStatusNotFound(w, r,
				fmt.Errorf("invalid directory name: %w", err))
			return
		}
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("invalid directory: %w", err))
			return
		}

		if err := dirStore.Delete(r.Context(), dir); err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("%s: %w", removeDirErrMsg, err))
			return
		}

		e := &core.EventData{
			Type: core.WatchDirRemoved,
			Data: dir,
		}
		if err := event.Publish(r.Context(), e); err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("%s: %w", removeDirErrMsg, err))
			return
		}

		handler.RespondWithStatusOK(w, r, dir)
	}
}
