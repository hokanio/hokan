package directories_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler/api"
)

func TestHandleDeleteDir(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	dirStore := mocks.NewMockDirectoryStore(controller)
	event := mocks.NewMockEventCreator(controller)
	pathID := ksuid.New().String()
	s := api.Server{
		Dirs:   dirStore,
		Events: event,
	}
	url := fmt.Sprintf("/directories/%s", pathID)

	t.Run("fail: dir not found", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(nil, core.ErrDirectoryNotFound)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", url, nil)

		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("fail: unexpected error", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(nil, assert.AnError)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", url, nil)

		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("fail: eror from db", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(&core.Directory{ID: pathID}, nil)

		dirStore.EXPECT().Delete(gomock.Any(), &core.Directory{ID: pathID}).
			Return(assert.AnError)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", url, nil)

		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("fail: eror from event publishing", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(&core.Directory{ID: pathID}, nil)

		dirStore.EXPECT().Delete(gomock.Any(), &core.Directory{ID: pathID}).
			Return(nil)

		event.EXPECT().Publish(gomock.Any(), &core.EventData{
			Type: core.WatchDirRemoved,
			Data: &core.Directory{ID: pathID},
		}).Return(assert.AnError)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", url, nil)

		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("success", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(&core.Directory{ID: pathID}, nil)

		dirStore.EXPECT().Delete(gomock.Any(), &core.Directory{ID: pathID}).
			Return(nil)

		event.EXPECT().Publish(gomock.Any(), &core.EventData{
			Type: core.WatchDirRemoved,
			Data: &core.Directory{ID: pathID},
		}).Return(nil)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", url, nil)

		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Code)
	})
}
