package files

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
)

func HandleList(dirStore core.DirectoryStore, fileStore core.FileStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		pathID := chi.URLParam(r, "pathID")
		_, err := dirStore.Find(r.Context(), &core.DirectorySearchOptions{ID: pathID})
		if errors.Is(err, core.ErrDirectoryNotFound) {
			handler.RespondWithStatusNotFound(w, r,
				fmt.Errorf("invalid directory name: %w", err))
			return
		}
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("invalid directory: %w", err))
			return
		}

		opt := &core.FileListOptions{}
		if q := r.URL.Query().Get("q"); q != "" {
			opt.Query = strings.ToLower(q)
		}

		files, err := fileStore.List(r.Context(), opt)
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("can't list files: %w", err))
			return
		}

		fileList := []core.FileResp{}
		for _, file := range files {
			fileList = append(fileList, core.FileResp{
				Restore:   fmt.Sprintf("%s/api/files/%s/restore", r.Host, file.ID),
				ID:        file.ID,
				Path:      file.Path,
				Info:      file.Info,
				IsDeleted: file.IsDeleted,
			})
		}

		render.Status(r, http.StatusOK)
		resp := &core.FilesListResp{
			Files: fileList,
			Links: createLinks(r),
			Meta: core.MetaDataResp{
				TotalItems: len(files),
			},
		}
		handler.RespondWithStatusOK(w, r, resp)
	}
}

func createLinks(r *http.Request) []core.LinksResp {
	return []core.LinksResp{
		{
			Rel:    "self",
			Href:   r.URL.EscapedPath(),
			Method: r.Method,
		},
	}
}
