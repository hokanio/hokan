package directories

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/directory"
	"gitlab.com/hokanio/hokan/pkg/handler"
)

const invalidDirectoryErr = "invalid directory"

func HandleFind(dirStore core.DirectoryStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		pathID := chi.URLParam(r, "pathID")

		dir, err := dirStore.Find(r.Context(), &core.DirectorySearchOptions{ID: pathID})
		if errors.Is(err, core.ErrDirectoryNotFound) {
			handler.RespondWithStatusNotFound(w, r,
				fmt.Errorf("%s %w", invalidDirectoryErr, err))
			return
		}
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("%s %w", invalidDirectoryErr, err))
			return
		}

		stats, err := directory.Stats(dir.Path)
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("%s %w", invalidDirectoryErr, err))
			return
		}

		renderData := core.DirectoryDetails{
			Directory: *dir,
			Stats:     *stats,
			Links:     createDirectoryDetailsLinks(r),
		}

		handler.RespondWithStatusOK(w, r, renderData)
	}
}

func createDirectoryDetailsLinks(r *http.Request) []core.LinksResp {
	return []core.LinksResp{
		{
			Rel:    "self",
			Href:   r.URL.EscapedPath(),
			Method: http.MethodGet,
		},
		{
			Rel:    "delete",
			Href:   r.URL.EscapedPath(),
			Method: http.MethodDelete,
		},
		{
			Rel:    "files",
			Href:   r.URL.EscapedPath() + "/files",
			Method: http.MethodGet,
		},
	}
}
