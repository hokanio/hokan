package directories_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler/api"
	"gitlab.com/hokanio/hokan/pkg/testing/tools"
)

func Test_FindByPath(t *testing.T) {
	testPath, err := ioutil.TempDir(os.TempDir(), "")
	assert.NoError(t, err)
	defer os.RemoveAll(testPath)

	controller := gomock.NewController(t)
	defer controller.Finish()

	dirStore := mocks.NewMockDirectoryStore(controller)

	server := api.Server{
		Dirs: dirStore,
	}

	pathID := ksuid.New().String()
	url := fmt.Sprintf("/directories/%s", pathID)

	t.Run("fail: error from local storage", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(nil, assert.AnError)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("fail: path not found locally", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(nil, core.ErrDirectoryNotFound)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("fail: invalid directory", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(&core.Directory{
				ID:        pathID,
				Path:      "/bad/path",
				Recursive: true,
				Machine:   "test",
			}, nil)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("success", func(t *testing.T) {
		dirStore.EXPECT().Find(gomock.Any(), &core.DirectorySearchOptions{ID: pathID}).
			Return(&core.Directory{
				ID:        pathID,
				Path:      testPath,
				Recursive: true,
				Machine:   "test",
			}, nil)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Code)

		body := strings.TrimSpace(w.Body.String())

		tools.TestJSONPathNotEmpty(t, "directory.id", body)
		tools.TestJSONPath(t, testPath, "directory.path", body)
		tools.TestJSONPath(t, "0", "stats.total_files", body)
		tools.TestJSONPath(t, "1", "stats.total_dirs", body)
	})
}
