package directories

import (
	"fmt"
	"net/http"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
	"gitlab.com/hokanio/hokan/pkg/logger"
)

func HandleList(dirStore core.DirectoryStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logger.FromRequest(r)
		dirs, err := dirStore.List(r.Context())
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("can't list directories: %w", err))
			return
		}
		for _, dir := range dirs {
			l.Debugf("dir: %q\n", dir.Path)
		}
		resp := &core.DirectoriesListResp{
			Directories: dirs,
			Links:       createLinks(r),
			Meta: core.MetaDataResp{
				TotalItems: len(dirs),
			},
		}
		handler.RespondWithStatusOK(w, r, resp)
	}
}

func createLinks(r *http.Request) []core.LinksResp {
	return []core.LinksResp{
		{
			Rel:    "self",
			Href:   r.URL.EscapedPath(),
			Method: r.Method,
		},
		{
			Rel:    "addDir",
			Href:   r.URL.EscapedPath(),
			Method: "POST",
		},
		{
			Rel:    "getDir",
			Href:   r.URL.EscapedPath() + "/{dirID}",
			Method: "GET",
		},
	}
}
