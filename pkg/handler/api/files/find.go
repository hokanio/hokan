package files

import (
	"net/http"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
)

func HandleFind(fileStore core.FileStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler.RespondWithStatusOK(w, r, "")
	}
}
