package files

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
	"gitlab.com/hokanio/hokan/pkg/logger"
)

const (
	errPublishEvent = "can't publish RestoreFile event"
	errFileRestore  = "can't restore file"
)

func HandleRestore(fileStore core.FileStore, event core.EventCreator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logger.FromRequest(r)
		fileID := chi.URLParam(r, "fileID")
		l.Debugf("fileID is %s", fileID)

		file, err := fileStore.Find(r.Context(), &core.FileSearchOptions{
			ID: fileID,
		})
		if err != nil {
			handler.RespondWithStatusNotFound(w, r,
				fmt.Errorf("%s: %w", errFileRestore, err))
			return
		}

		err = event.Publish(r.Context(), &core.EventData{
			Type: core.RestoreFile,
			Data: *file,
		})
		if err != nil {
			handler.RespondWithStatusInternalServerError(w, r,
				fmt.Errorf("%s: %w", errPublishEvent, err))
			return
		}

		handler.RespondWithStatusOK(w, r, "OK")
	}
}
