package files_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler/api"
)

const (
	validFileID = "123"
)

func Test_HandleRestore(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	dirStore := mocks.NewMockDirectoryStore(controller)
	eventCreator := mocks.NewMockEventCreator(controller)
	fileStore := mocks.NewMockFileStore(controller)

	server := api.New(fileStore, dirStore, eventCreator)

	t.Run("faild with file not found error", func(t *testing.T) {
		fileStore.EXPECT().Find(gomock.Any(), &core.FileSearchOptions{
			ID: validFileID,
		}).Return(nil, core.ErrFileNotFound)

		w := httptest.NewRecorder()
		url := fmt.Sprintf("/files/%s/restore", validFileID)

		r := httptest.NewRequest("POST", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusNotFound, w.Code)
	})

	t.Run("faild with error from event pubisher", func(t *testing.T) {
		file := &core.File{
			ID:   validFileID,
			Path: "/test/file.txt",
		}

		fileStore.EXPECT().Find(gomock.Any(), &core.FileSearchOptions{
			ID: validFileID,
		}).Return(file, nil)

		eventCreator.EXPECT().Publish(gomock.Any(), &core.EventData{
			Type: core.RestoreFile,
			Data: *file,
		}).Return(assert.AnError)

		w := httptest.NewRecorder()
		url := fmt.Sprintf("/files/%s/restore", validFileID)

		r := httptest.NewRequest("POST", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("success", func(t *testing.T) {
		file := &core.File{
			ID:   validFileID,
			Path: "/test/file.txt",
		}

		fileStore.EXPECT().Find(gomock.Any(), &core.FileSearchOptions{
			ID: validFileID,
		}).Return(file, nil)

		eventCreator.EXPECT().Publish(gomock.Any(), &core.EventData{
			Type: core.RestoreFile,
			Data: *file,
		}).Return(nil)

		w := httptest.NewRecorder()
		url := fmt.Sprintf("/files/%s/restore", validFileID)

		r := httptest.NewRequest("POST", url, nil)

		server.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Code)
	})
}
