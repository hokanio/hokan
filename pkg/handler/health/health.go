package health

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
)

var corsOpts = cors.Options{
	AllowedOrigins:   []string{"*"},
	AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
	AllowedHeaders:   []string{"Accept", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Content-Type", "X-CSRF-Token"},
	ExposedHeaders:   []string{"Link"},
	AllowCredentials: true,
	MaxAge:           300,
}

// New returns a new health check router
func New() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.NoCache)
	r.Handle("/", Handler())
	return r
}

// Handler creates an http.HandlerFunc that performs system healthchecks
func Handler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// set Access-Control-Allow-Origin header for the electron UI
		w.Header().Set("Access-Control-Allow-Origin", "*")
		render.Status(r, 200)
		render.PlainText(w, r, "OK")
	}
}
