package handler

import (
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/logger"
)

func RespondWithStatusOK(w http.ResponseWriter, r *http.Request, v interface{}) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, v)
}

func RespondWithStatusCreated(w http.ResponseWriter, r *http.Request, v interface{}) {
	render.Status(r, http.StatusCreated)
	render.JSON(w, r, v)
}

func RespondWithStatusNoContent(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusNoContent)
	render.JSON(w, r, "")
}

func RespondWithStatusUnauthorized(w http.ResponseWriter, r *http.Request, err error) {
	respondWithStatus(err, http.StatusUnauthorized, w, r)
}

func RespondWithStatusBadRequest(w http.ResponseWriter, r *http.Request, err error) {
	respondWithStatus(err, http.StatusBadRequest, w, r)
}

func RespondWithStatusNotFound(w http.ResponseWriter, r *http.Request, err error) {
	respondWithStatus(err, http.StatusNotFound, w, r)
}

func RespondWithStatusInternalServerError(w http.ResponseWriter, r *http.Request, err error) {
	respondWithStatus(err, http.StatusInternalServerError, w, r)
}

func respondWithStatus(err error, code int, w http.ResponseWriter, r *http.Request) {
	logger.FromRequest(r).WithError(err).Error("error from rest api")
	render.Status(r, code)
	render.JSON(w, r, core.Resp{
		Msg:    err.Error(),
		Code:   code,
		Status: http.StatusText(code),
	})
}
