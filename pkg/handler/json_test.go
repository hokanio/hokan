package handler

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_RespondWithOK(t *testing.T) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	msg := "OK"
	RespondWithStatusOK(w, r, msg)
	assert.Equal(t, 200, w.Code)
}
