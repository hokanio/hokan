package web

import (
	"net/http"
	"os/user"
	"runtime"

	"github.com/go-chi/render"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/directory"
	"gitlab.com/hokanio/hokan/pkg/logger"
	"gitlab.com/hokanio/hokan/pkg/version"
)

const homeDirErrMsg = "can't find home directory"

func HandleInfo(_ core.ConfigStore, directoryStore core.DirectoryStore, backupOptions core.BackupOptions) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logger.FromRequest(r)
		userName := "user"
		u, err := user.Current()
		if err == nil {
			l.WithError(err).Error("can't get current user")
			userName = u.Name
		}

		userHome, err := directory.NewHome()
		if err != nil {
			l.WithError(err).Error(homeDirErrMsg)
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, core.Resp{
				Code: http.StatusInternalServerError,
				Msg:  homeDirErrMsg,
			})
			return
		}

		userFolders := userHome.UserFolders()
		backupFolders, err := directoryStore.List(r.Context())
		if err != nil {
			l.WithError(err).Error("can't read from directoryStore")
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, core.Resp{
				Code: http.StatusInternalServerError,
				Msg:  "can't read from directoryStore",
			})
			return
		}

		// check if any of the user folders are in the backup
		for _, dir := range backupFolders {
			for _, userFolder := range userFolders {
				if dir.Path == userFolder.Path {
					userFolder.InBackup = true
				}
			}
		}

		info := &core.Info{
			Machine:     backupOptions.LocalMachineName,
			OS:          runtime.GOOS,
			User:        userName,
			Version:     version.Version.String(),
			UserFolders: userFolders,
		}
		render.Status(r, http.StatusOK)
		render.JSON(w, r, info)
	}
}
