package web_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler/web"
	"gitlab.com/hokanio/hokan/pkg/testing/tools"
)

func TestHandleInfo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	configStore := mocks.NewMockConfigStore(controller)
	directoryStore := mocks.NewMockDirectoryStore(controller)
	backupOptions := core.BackupOptions{
		LocalMachineName: "test",
	}

	s := web.New(configStore, directoryStore, backupOptions)

	t.Run("fail: error from directory storage", func(t *testing.T) {
		directoryStore.EXPECT().List(gomock.Any()).
			Return([]core.Directory{}, assert.AnError)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/info", nil)
		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Code)
	})

	t.Run("success", func(t *testing.T) {
		directoryStore.EXPECT().List(gomock.Any()).
			Return([]core.Directory{}, nil)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/info", nil)
		s.Handler().ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Code)

		body := strings.TrimSpace(w.Body.String())
		tools.TestJSONPathNotEmpty(t, "machine", body)
		tools.TestJSONPathNotEmpty(t, "os", body)
		tools.TestJSONPathNotEmpty(t, "version", body)
	})
}
