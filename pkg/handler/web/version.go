package web

import (
	"net/http"

	"gitlab.com/hokanio/hokan/pkg/handler"
	"gitlab.com/hokanio/hokan/pkg/version"
)

func HandleVersion(w http.ResponseWriter, r *http.Request) {
	v := struct {
		Source  string `json:"source,omitempty"`
		Version string `json:"version,omitempty"`
		Commit  string `json:"commit,omitempty"`
	}{
		Source:  version.GitRepository,
		Commit:  version.GitCommit,
		Version: version.Version.String(),
	}

	handler.RespondWithStatusOK(w, r, v)
}
