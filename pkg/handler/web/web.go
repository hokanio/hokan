package web

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/handler"
)

var corsOpts = cors.Options{
	AllowedOrigins:   []string{"*"},
	AllowedMethods:   []string{"GET", "OPTIONS"},
	AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
	ExposedHeaders:   []string{"Link"},
	AllowCredentials: true,
	MaxAge:           300,
}

type Server struct {
	configStore    core.ConfigStore
	directoryStore core.DirectoryStore
	backupOptions  core.BackupOptions
}

func New(configStore core.ConfigStore, directoryStore core.DirectoryStore, backupOptions core.BackupOptions) *Server {
	return &Server{
		configStore:    configStore,
		directoryStore: directoryStore,
		backupOptions:  backupOptions,
	}
}

func (s *Server) Handler() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.NoCache)

	cors := cors.New(corsOpts)
	r.Use(cors.Handler)

	r.Get("/version", HandleVersion)
	r.Get("/info", HandleInfo(s.configStore, s.directoryStore, s.backupOptions))
	r.Get("/", RootHandler())

	return r
}

func RootHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		links := []core.LinksResp{
			{
				Rel:    "self",
				Href:   r.URL.EscapedPath(),
				Method: "GET",
			},
			{
				Rel:    "version",
				Href:   "/version",
				Method: "GET",
			},
			{
				Rel:    "health",
				Href:   "/healthz",
				Method: "GET",
			},
			{
				Rel:    "info",
				Href:   "/info",
				Method: "GET",
			},
			{
				Rel:    "api",
				Href:   "/api",
				Method: "GET",
			},
		}
		renderData := &core.APIListResp{
			Links: links,
		}
		handler.RespondWithStatusOK(w, r, renderData)
	}
}

// func HandleAPIList() http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		links := []core.LinksResp{
// 			{
// 				Rel:    "self",
// 				Href:   r.URL.EscapedPath(),
// 				Method: "GET",
// 			},
// 			{
// 				Rel:    "localDirs",
// 				Href:   "/api/directories",
// 				Method: "GET",
// 			},
// 			{
// 				Rel:    "version",
// 				Href:   "/version",
// 				Method: "GET",
// 			},
// 			{
// 				Rel:    "health",
// 				Href:   "/healthz",
// 				Method: "GET",
// 			},
// 		}
// 		renderData := &core.APIListResp{
// 			Links: links,
// 		}
// 		handler.RespondWithStatusOK(w, r, renderData)
// 	}
// }
