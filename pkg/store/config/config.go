package config

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/store/db"
)

var bucketName = "watch:config"

type configStore struct {
	db core.DB
}

func New(database core.DB) core.ConfigStore {
	return &configStore{
		db: database,
	}
}

func (s *configStore) Get(ctx context.Context, name string) (*core.Config, error) {
	config := &core.Config{}

	value, err := s.db.Read(bucketName, name)
	if errors.Is(err, &db.ErrBucketNotFound{}) || value == nil {
		return nil, core.ErrConfigNotFound
	}
	if err != nil {
		return nil, err
	}

	if err := json.NewDecoder(bytes.NewReader(value)).Decode(config); err != nil {
		return nil, err
	}
	return config, nil
}

func (s *configStore) Create(ctx context.Context, conf *core.Config) error {
	var value bytes.Buffer
	if err := json.NewEncoder(&value).Encode(conf); err != nil {
		return err
	}
	return s.db.Write(bucketName, conf.StorageName, value.String())
}

func (s *configStore) Update(ctx context.Context, conf *core.Config) error {
	return core.ErrNotImplemented
}

func (s *configStore) Delete(ctx context.Context, conf *core.Config) error {
	return core.ErrNotImplemented
}
