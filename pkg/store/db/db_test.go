package db

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	testBucketName  = "test:bucket"
	testKey         = "foo"
	testValue       = "bar"
	testWrongKey    = "xxx"
	testWrongBucket = "wrong:bucket"
)

func TestBolt_WriteRead(t *testing.T) {
	tmpDir, err := ioutil.TempDir(os.TempDir(), "")
	assert.NoError(t, err)
	dbFile := path.Join(tmpDir, "test.db")
	defer os.RemoveAll(tmpDir)

	conn, err := Connect(dbFile)
	assert.NoError(t, err)

	err = conn.Write(testBucketName, testKey, testValue)
	assert.NoError(t, err)

	t.Run("read from wrong bucket", func(t *testing.T) {
		_, err = conn.Read(testWrongBucket, testWrongKey)
		assert.EqualError(t, err, `db.Read: bucket "wrong:bucket" was not found`)
	})

	t.Run("read from wrong key", func(t *testing.T) {
		val, err := conn.Read(testBucketName, testWrongKey)
		assert.NoError(t, err)
		assert.Empty(t, val)
	})

	t.Run("success: read", func(t *testing.T) {
		val, err := conn.Read(testBucketName, testKey)
		assert.NoError(t, err)
		assert.Equal(t, testValue, string(val))
	})

	t.Run("delete error", func(t *testing.T) {
		err := conn.Delete(testWrongBucket, testKey)
		assert.EqualError(t, err, `db.Delete: bucket "wrong:bucket" was not found`)
	})
}
