package directory

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/segmentio/ksuid"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/store/db"
)

var (
	bucketName           = "watch:directories"
	dirPathToIDMapBucket = "dir:path:id"
)

type directoryStore struct {
	db          core.DB
	storageName string
}

func New(database core.DB, storageName string) core.DirectoryStore {
	return &directoryStore{
		db:          database,
		storageName: storageName,
	}
}

// List returns a list of all directories in the watch list.
func (s *directoryStore) List(ctx context.Context) ([]core.Directory, error) {
	dirs := []core.Directory{}

	data, err := s.db.ReadBucket(bucketName, &core.ReadBucketOptions{})
	if err != nil {
		if _, ok := err.(*db.ErrBucketNotFound); ok {
			return dirs, nil
		}
		return nil, err
	}

	for _, entry := range data {
		dir := core.Directory{}
		err := json.NewDecoder(strings.NewReader(entry.Value)).Decode(&dir)
		if err != nil {
			return nil, err
		}
		dirs = append(dirs, dir)
	}

	return dirs, nil
}

// Find returns a directory entry by it's id.
func (s *directoryStore) Find(ctx context.Context, opt *core.DirectorySearchOptions) (*core.Directory, error) {
	if opt == nil {
		return nil, core.ErrEmptySearchOptions
	}
	dir := &core.Directory{}

	var dirID string
	if opt.Path != "" {
		// check if file already stored
		path := path.Clean(opt.Path)
		val, err := s.db.Read(dirPathToIDMapBucket, path)
		if errors.Is(err, &db.ErrBucketNotFound{}) || val == nil {
			return nil, core.ErrDirectoryNotFound
		}
		if err != nil {
			return nil, fmt.Errorf("dir.find: can't read key=%q from bucket='%s': %w", path, dirPathToIDMapBucket, err)
		}
		dirID = string(val)
	} else {
		dirID = opt.ID
	}

	value, err := s.db.Read(bucketName, dirID)
	if errors.Is(err, &db.ErrBucketNotFound{}) || value == nil {
		return nil, core.ErrDirectoryNotFound
	}
	if err != nil {
		return nil, fmt.Errorf("dir.find: can't read key=%q from bucket='%s': %w", dirID, bucketName, err)
	}

	err = json.NewDecoder(bytes.NewReader(value)).Decode(dir)
	return dir, err
}

func (s *directoryStore) Delete(ctx context.Context, dir *core.Directory) error {
	findDir, err := s.Find(ctx, &core.DirectorySearchOptions{
		Path: dir.Path,
		ID:   dir.ID,
	})
	if err != nil {
		return err
	}

	if err := s.db.Delete(dirPathToIDMapBucket, findDir.Path); err != nil {
		return err
	}
	return s.db.Delete(bucketName, findDir.ID)
}

func (s *directoryStore) Create(ctx context.Context, dir *core.Directory) error {
	if _, err := os.Stat(dir.Path); os.IsNotExist(err) {
		return core.ErrDirectoryNotFound
	}

	dir.ID = ksuid.New().String()

	if dir.StorageName == "" {
		dir.StorageName = s.storageName
	}

	var value bytes.Buffer
	if err := json.NewEncoder(&value).Encode(dir); err != nil {
		return err
	}

	if err := s.db.Write(dirPathToIDMapBucket, dir.Path, dir.ID); err != nil {
		return err
	}

	return s.db.Write(bucketName, dir.ID, value.String())
}
