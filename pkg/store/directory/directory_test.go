package directory

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	storedb "gitlab.com/hokanio/hokan/pkg/store/db"
)

const testStorageName = "test"

func Test_directoryStore_List(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	db := mocks.NewMockDB(controller)

	t.Run("fail: bucket not found", func(t *testing.T) {
		db.EXPECT().ReadBucket(bucketName, &core.ReadBucketOptions{}).
			Return(nil, &storedb.ErrBucketNotFound{})

		s := directoryStore{db, testStorageName}
		dirs, err := s.List(context.TODO())
		assert.NoError(t, err)
		assert.Equal(t, 0, len(dirs))
	})

	t.Run("fail: error from storage", func(t *testing.T) {
		db.EXPECT().ReadBucket(bucketName, &core.ReadBucketOptions{}).
			Return(nil, assert.AnError)

		s := directoryStore{db, testStorageName}
		_, err := s.List(context.TODO())
		assert.Error(t, err, assert.AnError)
	})

	t.Run("success", func(t *testing.T) {
		db.EXPECT().ReadBucket(bucketName, &core.ReadBucketOptions{}).
			Return([]core.BucketData{
				{
					Key:   "foo",
					Value: `{"active":true,"path":"/foo"}`,
				},
			}, nil)

		s := directoryStore{db, testStorageName}
		dirs, err := s.List(context.TODO())
		assert.Equal(t, "/foo", dirs[0].Path)
		assert.NoError(t, err)
	})
}

func Test_directoryStore_Find(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	db := mocks.NewMockDB(controller)
	testPath := "/foo/bar"
	testDirID := "287RRlln03guR13InYDoJvsL1Ir"

	t.Run("fail: empty search options", func(t *testing.T) {
		s := directoryStore{db, testStorageName}
		_, err := s.Find(context.TODO(), nil)
		assert.Error(t, err, core.ErrEmptySearchOptions)
	})

	t.Run("fail: directory by path not found: BucketNotFound", func(t *testing.T) {
		db.EXPECT().Read(dirPathToIDMapBucket, testPath).
			Return(nil, &storedb.ErrBucketNotFound{})

		s := directoryStore{db, testStorageName}
		_, err := s.Find(context.TODO(), &core.DirectorySearchOptions{Path: testPath})
		assert.Error(t, err, core.ErrDirectoryNotFound)
	})

	t.Run("fail: error from storage", func(t *testing.T) {
		db.EXPECT().Read(dirPathToIDMapBucket, testPath).
			Return(nil, assert.AnError)

		s := directoryStore{db, testStorageName}
		_, err := s.Find(context.TODO(), &core.DirectorySearchOptions{Path: testPath})
		assert.Error(t, err, assert.AnError)
	})

	t.Run("fail: json decode error", func(t *testing.T) {
		db.EXPECT().Read(dirPathToIDMapBucket, testPath).
			Return([]byte(testDirID), nil)

		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"machine":"test123","path":"/foo/bar"`), nil)

		s := directoryStore{db, testStorageName}
		_, err := s.Find(context.TODO(), &core.DirectorySearchOptions{Path: testPath})
		assert.Error(t, err)
	})

	t.Run("fail: path not found", func(t *testing.T) {
		db.EXPECT().Read(dirPathToIDMapBucket, testPath).
			Return([]byte(testDirID), nil)

		db.EXPECT().Read(bucketName, testDirID).
			Return(nil, nil)

		s := directoryStore{db, testStorageName}
		_, err := s.Find(context.TODO(), &core.DirectorySearchOptions{Path: testPath})
		assert.Error(t, err, core.ErrDirectoryNotFound)
	})

	t.Run("success: find directory by pathID", func(t *testing.T) {
		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"machine":"test123","path":"/foo/bar"}`), nil)

		s := directoryStore{db, testStorageName}
		dir, err := s.Find(context.TODO(), &core.DirectorySearchOptions{ID: testDirID})
		assert.Equal(t, testPath, dir.Path)
		assert.Equal(t, "test123", dir.Machine)
		assert.NoError(t, err)
	})

	t.Run("success: find directory by path", func(t *testing.T) {
		db.EXPECT().Read(dirPathToIDMapBucket, testPath).
			Return([]byte(testDirID), nil)

		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"machine":"test123","path":"/foo/bar"}`), nil)

		s := directoryStore{db, testStorageName}
		dir, err := s.Find(context.TODO(), &core.DirectorySearchOptions{Path: testPath})
		assert.Equal(t, testPath, dir.Path)
		assert.Equal(t, "test123", dir.Machine)
		assert.NoError(t, err)
	})
}

func Test_directoryStore_Create(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	db := mocks.NewMockDB(controller)
	s := directoryStore{db, testStorageName}

	t.Run("fail: directory not found", func(t *testing.T) {
		err := s.Create(context.TODO(), &core.Directory{Path: "bad/path"})
		assert.Error(t, err)
		assert.ErrorIs(t, err, core.ErrDirectoryNotFound)
	})

	t.Run("success", func(t *testing.T) {
		db.EXPECT().Write(dirPathToIDMapBucket, ".", gomock.Any()).
			Return(nil)

		db.EXPECT().Write(bucketName, gomock.Any(), gomock.Any()).
			Return(nil)

		s := directoryStore{db, testStorageName}
		err := s.Create(context.TODO(), &core.Directory{Path: "."})
		assert.NoError(t, err)
	})
}

func Test_directoryDelete(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	testDirID := "287b3gCrbx3JG3oOqb96PP37T8w"

	db := mocks.NewMockDB(controller)

	t.Run("fail: dir not found", func(t *testing.T) {
		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(""), assert.AnError)

		s := directoryStore{db, testStorageName}
		err := s.Delete(context.TODO(), &core.Directory{ID: testDirID})
		assert.Error(t, err)
	})

	t.Run("fail: error from delete from map", func(t *testing.T) {
		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"id":"287b3gCrbx3JG3oOqb96PP37T8w","path":"/foo/bar"}`), nil)

		db.EXPECT().Delete(dirPathToIDMapBucket, "/foo/bar").
			Return(assert.AnError)

		s := directoryStore{db, testStorageName}
		err := s.Delete(context.TODO(), &core.Directory{ID: testDirID})
		assert.Error(t, err)
	})

	t.Run("fail: error from delete from file bucket", func(t *testing.T) {
		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"id":"287b3gCrbx3JG3oOqb96PP37T8w","path":"/foo/bar"}`), nil)

		db.EXPECT().Delete(dirPathToIDMapBucket, "/foo/bar").
			Return(nil)

		db.EXPECT().Delete(bucketName, testDirID).
			Return(assert.AnError)

		s := directoryStore{db, testStorageName}
		err := s.Delete(context.TODO(), &core.Directory{ID: testDirID})
		assert.Error(t, err)
	})

	t.Run("success", func(t *testing.T) {
		db.EXPECT().Read(bucketName, testDirID).
			Return([]byte(`{"id":"287b3gCrbx3JG3oOqb96PP37T8w","path":"/foo/bar"}`), nil)

		db.EXPECT().Delete(dirPathToIDMapBucket, "/foo/bar").
			Return(nil)

		db.EXPECT().Delete(bucketName, testDirID).
			Return(nil)

		s := directoryStore{db, testStorageName}
		err := s.Delete(context.TODO(), &core.Directory{ID: testDirID})
		assert.NoError(t, err)
	})
}
