package file

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"path"
	"strings"

	"github.com/segmentio/ksuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/store/db"
)

const (
	filePathToIDMapBucket = "file:path:id"
	bucketName            = "file:backup"
)

type fileStore struct {
	db core.DB
}

func New(database core.DB) core.FileStore {
	return &fileStore{
		db: database,
	}
}

func (s *fileStore) List(ctx context.Context, opt *core.FileListOptions) ([]core.File, error) {
	if opt == nil {
		return nil, fmt.Errorf("empty list options")
	}
	var files []core.File

	data, err := s.db.ReadBucket(bucketName, &core.ReadBucketOptions{
		Offset: opt.Offset,
		Limit:  opt.Limit,
	})
	if errors.Is(err, &db.ErrBucketNotFound{}) {
		return nil, fmt.Errorf("bucket %s not found", bucketName)
	}
	if err != nil {
		return nil, err
	}

	for _, entry := range data {
		file := core.File{}
		err := json.NewDecoder(strings.NewReader(entry.Value)).Decode(&file)
		if err != nil {
			return nil, err
		}
		if opt.Query != "" && !strings.Contains(strings.ToLower(file.Path), opt.Query) {
			continue
		}
		files = append(files, file)
	}

	return files, nil
}

func (s *fileStore) Find(ctx context.Context, opt *core.FileSearchOptions) (*core.File, error) {
	if opt == nil {
		return nil, fmt.Errorf("empty search options")
	}
	file := &core.File{}

	var fileID string
	if opt.FilePath != "" {
		// check if file already stored
		path := path.Clean(opt.FilePath)
		val, err := s.db.Read(filePathToIDMapBucket, path)
		if errors.Is(err, &db.ErrBucketNotFound{}) || val == nil {
			return nil, core.ErrFileNotFound
		}
		if err != nil {
			return nil, fmt.Errorf("file.find: can't read key=%q from bucket='%s': %w", path, filePathToIDMapBucket, err)
		}
		fileID = string(val)
	} else {
		fileID = opt.ID
	}

	value, err := s.db.Read(bucketName, fileID)
	if errors.Is(err, &db.ErrBucketNotFound{}) || value == nil {
		return nil, core.ErrFileNotFound
	}
	if err != nil {
		return nil, fmt.Errorf("file.find: can't read key=%q from bucket='%s': %w", fileID, bucketName, err)
	}

	err = json.NewDecoder(bytes.NewReader(value)).Decode(file)
	return file, err
}

// Save saves meta information for a given file to the specific bucket.
// bucketName is defined by the backend
func (s *fileStore) Save(ctx context.Context, file *core.File) error {
	// log.Printf("file.Save() %q\n", file.Path)
	if file.ID == "" {
		file.ID = ksuid.New().String()
	} else {
		if _, err := ksuid.Parse(file.ID); err != nil {
			return core.ErrInvalidFileID
		}
	}

	var value bytes.Buffer
	if err := json.NewEncoder(&value).Encode(file); err != nil {
		return err
	}

	path := path.Clean(file.Path)
	if err := s.db.Write(filePathToIDMapBucket, path, file.ID); err != nil {
		return err
	}

	return s.db.Write(bucketName, file.ID, value.String())
}

func (s *fileStore) Delete(ctx context.Context, f *core.File) error {
	log.Printf("file.Delete() %+v", f)
	file, err := s.Find(ctx, &core.FileSearchOptions{
		FilePath: f.Path,
	})
	if err != nil {
		return err
	}

	// mark as deleted and write back, it will be overwritten
	file.IsDeleted = true
	var value bytes.Buffer
	if err := json.NewEncoder(&value).Encode(file); err != nil {
		return err
	}

	return s.db.Write(bucketName, file.ID, value.String())
}
