package file

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/store/db"
	"gitlab.com/hokanio/hokan/pkg/testing/tools"
	"gitlab.com/hokanio/hokan/pkg/watcher/utils"
)

var testFilePath = "testdata/test.txt"

func getTestingFile(t *testing.T) string {
	pwd, err := os.Getwd()
	assert.NoError(t, err)
	return filepath.Join(pwd, testFilePath)
}

func TestSaveFindDeleteFile(t *testing.T) {
	filePath := "/foo/bar/test.jpg"
	checksum := "abcXYZ"

	tmpDir, err := ioutil.TempDir(os.TempDir(), "")
	assert.NoError(t, err)
	dbFile := path.Join(tmpDir, "test.db")
	defer os.RemoveAll(tmpDir)

	storage, err := db.Connect(dbFile)
	assert.NoError(t, err)
	assert.NotNil(t, storage)
	fileStore := New(storage)
	assert.NotEmpty(t, fileStore)

	file := &core.File{
		Path:     filePath,
		Checksum: checksum,
	}
	err = fileStore.Save(context.TODO(), file)
	assert.NoError(t, err)

	fileFind, err := fileStore.Find(context.TODO(), &core.FileSearchOptions{
		FilePath: filePath,
	})
	assert.NoError(t, err)
	assert.Equal(t, checksum, fileFind.Checksum)
	assert.Equal(t, filePath, fileFind.Path)
	assert.False(t, fileFind.IsDeleted)

	err = fileStore.Delete(context.TODO(), fileFind)
	assert.NoError(t, err)

	fileFindDel, err := fileStore.Find(context.TODO(), &core.FileSearchOptions{
		FilePath: filePath,
	})
	assert.NoError(t, err)
	assert.Equal(t, checksum, fileFindDel.Checksum)
	assert.Equal(t, filePath, fileFindDel.Path)
	assert.True(t, fileFindDel.IsDeleted)
}

func Test_fileStore_Save(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	db := mocks.NewMockDB(controller)
	testFilePath := getTestingFile(t)
	store := New(db)

	t.Run("fail: bad file id", func(t *testing.T) {
		file := &core.File{
			ID:   "badID",
			Path: testFilePath,
		}

		err := store.Save(context.TODO(), file)
		assert.ErrorIs(t, err, core.ErrInvalidFileID)
	})

	t.Run("success", func(t *testing.T) {
		db.EXPECT().Write(gomock.Any(), gomock.Any(), gomock.Any()).
			Do(func(bucketName, key, value string) error {
				assert.Equal(t, filePathToIDMapBucket, bucketName)
				assert.Contains(t, testFilePath, key)
				return nil
			})

		db.EXPECT().Write(gomock.Any(), gomock.Any(), gomock.Any()).
			Do(func(bucketName, key, value string) error {
				tools.TestJSONPath(t, "5e2bf57d3f40c4b6df69daf1936cb766f832374b4fc0259a7cbff06e2f70f269", "checksum", value)
				tools.TestJSONPath(t, "test.txt", "info.name", value)
				tools.TestJSONPath(t, "11", "info.size", value)
				return nil
			})

		checksum, info, err := utils.FileChecksumInfo(testFilePath)
		assert.NoError(t, err)

		file := &core.File{
			Path:     testFilePath,
			Checksum: checksum,
			Info:     info,
		}

		err = store.Save(context.TODO(), file)
		assert.NoError(t, err)
	})
}

func TestListOffsetLimit(t *testing.T) {
	t.Skip()
	tmpDir, err := ioutil.TempDir(os.TempDir(), "")
	assert.NoError(t, err)
	dbFile := path.Join(tmpDir, "test.db")
	defer os.RemoveAll(tmpDir)

	storage, err := db.Connect(dbFile)
	assert.NoError(t, err)
	assert.NotNil(t, storage)

	_, info, err := utils.FileChecksumInfo(dbFile)
	assert.NoError(t, err)

	fileStore := New(storage)
	for i := 0; i < 1003; i++ {
		name := fmt.Sprintf("/test/foo/file_%04d.png", i)
		file := &core.File{
			Path:     name,
			Checksum: "abc",
			Info:     info,
		}
		err := fileStore.Save(context.TODO(), file)
		assert.NoError(t, err)
	}

	tests := []struct {
		name         string
		opt          *core.FileListOptions
		expectedData []string
	}{
		{
			name: "case 1",
			opt: &core.FileListOptions{
				Offset: 0,
				Limit:  5,
			},
			expectedData: []string{
				"/test/foo/file_0000.png",
				"/test/foo/file_0001.png",
				"/test/foo/file_0002.png",
				"/test/foo/file_0003.png",
				"/test/foo/file_0004.png",
			},
		},
		{
			name: "case 2",
			opt: &core.FileListOptions{
				Offset: 5,
				Limit:  5,
			},
			expectedData: []string{
				"/test/foo/file_0005.png",
				"/test/foo/file_0006.png",
				"/test/foo/file_0007.png",
				"/test/foo/file_0008.png",
				"/test/foo/file_0009.png",
			},
		},
		{
			name: "case 3",
			opt: &core.FileListOptions{
				Offset: 100,
				Limit:  3,
			},
			expectedData: []string{
				"/test/foo/file_0100.png",
				"/test/foo/file_0101.png",
				"/test/foo/file_0102.png",
			},
		},
		{
			name: "case 4",
			opt: &core.FileListOptions{
				Offset: 1000,
				Limit:  5,
			},
			expectedData: []string{
				"/test/foo/file_1000.png",
				"/test/foo/file_1001.png",
				"/test/foo/file_1002.png",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data, err := fileStore.List(context.TODO(), tt.opt)
			assert.NoError(t, err)
			files := []string{}
			for _, f := range data {
				files = append(files, f.Path)
			}
			assert.Equal(t, tt.expectedData, files)
		})
	}
}
