package watcher

import (
	log "github.com/sirupsen/logrus"
	notify "gitlab.com/hokanio/notify/core"

	"gitlab.com/hokanio/hokan/pkg/core"
)

func (w *Watch) StartDirWatcher() {
	log.Debug("watcher.StartDirWatcher(): starting subscriber")
	ctx := w.ctx
	eventData := w.event.Subscribe(ctx, core.WatchDirStart)
	wg.Done()
	for {
		select {
		case <-ctx.Done():
			log.Info("watcher.StartDirWatcher(): event-stream canceled")
			return
		case e := <-eventData:
			data, ok := e.Data.(core.Directory)
			if !ok {
				log.WithField("caller", "watcher.StartDirWatcher()").
					Errorf("wrong event data: %#v", e)
				continue
			}
			w.notifier.StartWatching(data.Path, &notify.WatchingOptions{Rescan: true})
		}
	}
}

func (w *Watch) StartRescanWatcher() {
	log.Debug("watcher.StartRescanWatcher(): starting subscriber")
	ctx := w.ctx
	eventData := w.event.Subscribe(ctx, core.WatchDirRescan)
	wg.Done()

	for {
		select {
		case <-ctx.Done():
			log.Info("watcher.StartRescanWatcher(): event-stream canceled")
			return
		case data := <-eventData:
			log.Printf("watcher. StartRescanWatcher(): dir rescan event: %#v", data)
			w.notifier.RescanAll()
		}
	}
}

func (w *Watch) GetDirsToWatch() error {
	log.Debug("watcher.GetDirsToWatch()")
	dirs, err := w.store.List(w.ctx)
	if err != nil {
		log.WithError(err).Print("Can't list all directories")
		return err
	}
	for _, dir := range dirs {
		log.Printf("watcher.GetDirsToWatch(): publish %#v", dir)
		err = w.event.Publish(w.ctx, &core.EventData{
			Type: core.WatchDirStart,
			Data: dir,
		})
		if err != nil {
			log.WithError(err).Print("Can't publish [WatchDirStart] event")
		}
	}
	return nil
}
