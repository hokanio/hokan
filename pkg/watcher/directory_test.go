package watcher

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/event"
)

func TestWatch_StartDirWatcher(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	wg.Add(1)
	e := event.New(event.Config{})

	notifier := mocks.NewMockNotifier(controller)
	notifier.EXPECT().StartWatching("/foo", gomock.Any())

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    e,
		notifier: notifier,
	}

	go w.StartDirWatcher()
	wg.Wait()

	err := w.event.Publish(ctx, &core.EventData{
		Type: core.WatchDirStart,
		Data: core.Directory{
			Path: "/foo",
		},
	})
	assert.NoError(t, err)

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}

func TestWatch_StartRescanWatcher(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	wg.Add(1)
	e := event.New(event.Config{})

	notifier := mocks.NewMockNotifier(controller)
	notifier.EXPECT().RescanAll()

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    e,
		notifier: notifier,
	}

	go w.StartRescanWatcher()
	wg.Wait()

	err := w.event.Publish(ctx, &core.EventData{
		Type: core.WatchDirRescan,
	})
	assert.NoError(t, err)

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}

func TestWatch_GetDirsToWatch(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	store := mocks.NewMockDirectoryStore(controller)
	store.EXPECT().List(gomock.Any()).Return([]core.Directory{
		{
			ID:   "1",
			Path: "/foo",
		},
		{
			ID:   "2",
			Path: "/bar",
		},
	}, nil)
	notifier := mocks.NewMockNotifier(controller)
	e := mocks.NewMockEventCreator(controller)
	e.EXPECT().Publish(gomock.Any(), &core.EventData{
		Type: core.WatchDirStart,
		Data: core.Directory{
			ID:   "1",
			Path: "/foo",
		},
	}).Return(nil)
	e.EXPECT().Publish(gomock.Any(), &core.EventData{
		Type: core.WatchDirStart,
		Data: core.Directory{
			ID:   "2",
			Path: "/bar",
		},
	}).Return(nil)

	ctx := context.Background()
	w := &Watch{
		event:    e,
		ctx:      ctx,
		store:    store,
		notifier: notifier,
	}
	err := w.GetDirsToWatch()
	assert.NoError(t, err)
}
