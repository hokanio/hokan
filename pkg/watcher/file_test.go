package watcher

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	notifyEvent "gitlab.com/hokanio/notify/event"

	"gitlab.com/hokanio/hokan/mocks"
	"gitlab.com/hokanio/hokan/pkg/core"
	"gitlab.com/hokanio/hokan/pkg/watcher/utils/testnotify"
)

const (
	testFilePath      = "file_test.go"
	testFileIgnore    = "file.tmp"
	testIgnorePattern = ".tmp"
	errMsg            = "test error"
)

func TestWatch_StartFileWatcherFileAdded(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	pwd, err := os.Getwd()
	assert.NoError(t, err)

	t.Run("file ignored", func(t *testing.T) {
		event := mocks.NewMockEventCreator(controller)
		event.EXPECT().Publish(gomock.Any(), gomock.Any()).Times(0)

		sse := mocks.NewMockServerSideEventCreator(controller)
		sse.EXPECT().PublishMessage(gomock.Any()).Times(0)

		notifier := testnotify.New()

		ctx := context.Background()
		w := &Watch{
			ctx:      ctx,
			event:    event,
			notifier: notifier,
			catalog: []core.Directory{
				{
					Path: pwd,
				},
			},
			sse: sse,
			config: core.WatcherConfig{
				IgnoreFiles: []string{testIgnorePattern},
			},
		}

		go w.StartFileWatcher()
		notifier.StartWatching(testFileIgnore, nil)

		time.Sleep(100 * time.Millisecond)
		ctx.Done()
	})

	t.Run("file added", func(t *testing.T) {
		localPath := filepath.Join(pwd, testFilePath)
		event := mocks.NewMockEventCreator(controller)
		event.EXPECT().Publish(gomock.Any(), gomock.Any()).
			Do(func(_ context.Context, e *core.EventData) error {
				data, ok := e.Data.(core.File)
				assert.True(t, ok)
				assert.Equal(t, localPath, data.Path)
				assert.Equal(t, core.FileAdded, e.Type)
				return nil
			})

		sse := mocks.NewMockServerSideEventCreator(controller)
		sse.EXPECT().PublishMessage(gomock.Any()).
			Do(func(msg string) {
				assert.Equal(t, fmt.Sprintf("[event] File %q added", localPath), msg)
			})

		notifier := testnotify.New()

		ctx := context.Background()
		w := &Watch{
			ctx:      ctx,
			event:    event,
			notifier: notifier,
			catalog: []core.Directory{
				{
					Path: pwd,
				},
			},
			sse: sse,
		}

		go w.StartFileWatcher()
		notifier.StartWatching(localPath, nil)
		time.Sleep(100 * time.Millisecond)
		ctx.Done()
	})
}

func TestWatch_StartFileWatcherFileChanged(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	pwd, err := os.Getwd()
	assert.NoError(t, err)
	localPath := filepath.Join(pwd, testFilePath)

	notifier := testnotify.New()
	event := mocks.NewMockEventCreator(controller)

	event.EXPECT().Publish(gomock.Any(), gomock.Any()).Do(func(_ context.Context, e *core.EventData) error {
		data, ok := e.Data.(core.File)
		assert.True(t, ok)
		assert.Equal(t, localPath, data.Path)
		assert.Equal(t, core.FileChanged, e.Type)
		return nil
	})

	sse := mocks.NewMockServerSideEventCreator(controller)
	sse.EXPECT().PublishMessage(gomock.Any()).Do(func(msg string) {
		assert.Equal(t, fmt.Sprintf("[event] File %q changed", localPath), msg)
	})

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    event,
		notifier: notifier,
		catalog: []core.Directory{
			{
				Path: pwd,
			},
		},
		sse: sse,
	}

	go w.StartFileWatcher()

	notifier.Event() <- notifyEvent.Event{
		Action: notifyEvent.FileModified,
		Path:   localPath,
	}

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}

func TestWatch_StartFileWatcherFileRemoved(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	pwd, err := os.Getwd()
	assert.NoError(t, err)
	localPath := filepath.Join(pwd, testFilePath)

	notifier := testnotify.New()
	event := mocks.NewMockEventCreator(controller)

	event.EXPECT().Publish(gomock.Any(), gomock.Any()).Do(func(_ context.Context, e *core.EventData) error {
		data, ok := e.Data.(core.File)
		assert.True(t, ok)
		assert.Equal(t, localPath, data.Path)
		assert.Equal(t, core.FileRemoved, e.Type)
		return nil
	})

	sse := mocks.NewMockServerSideEventCreator(controller)
	sse.EXPECT().PublishMessage(gomock.Any()).Do(func(msg string) {
		assert.Equal(t, fmt.Sprintf("[event] File %q removed", localPath), msg)
	})

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    event,
		notifier: notifier,
		catalog: []core.Directory{
			{
				Path: pwd,
			},
		},
		sse: sse,
	}

	go w.StartFileWatcher()

	notifier.Event() <- notifyEvent.Event{
		Action: notifyEvent.FileRemoved,
		Path:   localPath,
	}

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}

func TestWatch_StartFileWatcherFileRenamed(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	pwd, err := os.Getwd()
	assert.NoError(t, err)
	localPath := filepath.Join(pwd, testFilePath)
	localOldPath := filepath.Join(pwd, "old_test.go")

	notifier := testnotify.New()
	event := mocks.NewMockEventCreator(controller)

	event.EXPECT().Publish(gomock.Any(), gomock.Any()).Do(func(_ context.Context, e *core.EventData) error {
		data, ok := e.Data.(core.File)
		assert.True(t, ok)
		assert.Equal(t, localPath, data.Path)
		assert.Equal(t, localOldPath, data.OldPath)
		assert.Equal(t, core.FileRenamed, e.Type)
		return nil
	})

	sse := mocks.NewMockServerSideEventCreator(controller)
	sse.EXPECT().PublishMessage(gomock.Any()).Do(func(msg string) {
		assert.Equal(t, fmt.Sprintf("[event] File %q was renamed to %q", localOldPath, localPath), msg)
	})

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    event,
		notifier: notifier,
		catalog: []core.Directory{
			{
				Path: pwd,
			},
		},
		sse: sse,
	}

	go w.StartFileWatcher()

	notifier.Event() <- notifyEvent.Event{
		Action: notifyEvent.FileRenamedNewName,
		Path:   localPath,
		AdditionalInfo: notifyEvent.AdditionalInfo{
			OldName: localOldPath,
		},
	}

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}

func TestWatch_StartFileWatcherError(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	notifier := testnotify.New()
	event := mocks.NewMockEventCreator(controller)

	sse := mocks.NewMockServerSideEventCreator(controller)
	sse.EXPECT().PublishMessage(gomock.Any()).Do(func(msg string) {
		assert.Equal(t, fmt.Sprintf("[notifier] %q", errMsg), msg)
	})

	ctx := context.Background()
	w := &Watch{
		ctx:      ctx,
		event:    event,
		notifier: notifier,
		catalog:  []core.Directory{},
		sse:      sse,
	}

	go w.StartFileWatcher()

	notifier.Log() <- notifyEvent.Log{
		Level:   notifyEvent.ERROR.String(),
		Message: errMsg,
	}

	time.Sleep(100 * time.Millisecond)
	ctx.Done()
}
