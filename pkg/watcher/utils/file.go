package utils

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"

	"gitlab.com/hokanio/hokan/pkg/core"
)

func FileChecksumInfo(path string) (string, core.FileInfo, error) {
	file, erro := os.Open(path)
	if erro != nil {
		return "", core.FileInfo{}, erro
	}
	defer file.Close()
	info, errs := file.Stat()
	if errs != nil {
		return "", core.FileInfo{}, errs
	}

	if info.IsDir() {
		return "", core.FileInfo{}, fmt.Errorf("not a file")
	}

	h := sha256.New()
	if _, err := io.Copy(h, file); err != nil {
		return "", core.FileInfo{}, err
	}

	sum := fmt.Sprintf("%x", h.Sum(nil))
	return sum, core.FileInfo{
		Size:    info.Size(),
		Name:    info.Name(),
		ModTime: info.ModTime(),
	}, nil
}
