package testnotify

import (
	notifycore "gitlab.com/hokanio/notify/core"
	"gitlab.com/hokanio/notify/event"

	"gitlab.com/hokanio/hokan/pkg/core"
)

type FakeWatcher struct {
	events chan event.Event
	logs   chan event.Log
}

func New() core.Notifier {
	return &FakeWatcher{
		events: make(chan event.Event),
		logs:   make(chan event.Log),
	}
}

func (w *FakeWatcher) Event() chan event.Event {
	return w.events
}
func (w *FakeWatcher) Log() chan event.Log {
	return w.logs
}

func (w *FakeWatcher) RescanAll() {}

func (w *FakeWatcher) StopWatching(path string) {}

func (w *FakeWatcher) StartWatching(path string, _ *notifycore.WatchingOptions) {
	w.events <- event.Event{
		Action: event.FileAdded,
		Path:   path,
	}
}
