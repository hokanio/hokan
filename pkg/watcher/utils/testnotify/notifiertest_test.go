package testnotify

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/notify/event"
)

func TestFakeWatcher_StartWatching(t *testing.T) {
	w := New()
	go w.StartWatching("/test", nil)
	e := <-w.Event()
	assert.Equal(t, "/test", e.Path)
	assert.Equal(t, event.FileAdded, e.Action)
}
