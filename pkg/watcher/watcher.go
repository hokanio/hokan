package watcher

import (
	"context"
	"sync"

	"gitlab.com/hokanio/hokan/pkg/core"
)

var wg sync.WaitGroup

type Watch struct {
	ctx      context.Context
	event    core.EventCreator
	store    core.DirectoryStore
	notifier core.Notifier
	catalog  []core.Directory
	sse      core.ServerSideEventCreator
	config   core.WatcherConfig
}

func New(ctx context.Context,
	dirStore core.DirectoryStore,
	event core.EventCreator,
	notifier core.Notifier,
	sse core.ServerSideEventCreator,
	config core.WatcherConfig) (*Watch, error) {
	w := &Watch{
		ctx:      ctx,
		event:    event,
		store:    dirStore,
		notifier: notifier,
		sse:      sse,
		config:   config,
	}

	wg.Add(2) //nolint:gomnd
	go w.StartDirWatcher()
	go w.StartRescanWatcher()
	wg.Wait()
	err := w.GetDirsToWatch()
	if err != nil {
		return nil, err
	}

	dirs, err := dirStore.List(ctx)
	if err != nil {
		return nil, err
	}
	w.catalog = dirs

	go w.StartFileWatcher()
	return w, nil
}
